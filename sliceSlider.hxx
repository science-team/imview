/*
 * $Id: sliceSlider.hxx,v 1.2 2003/05/30 13:02:11 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A simple linked slider/value panel
 *
 * Hugues Talbot	29 May 2003
 *      
 *-----------------------------------------------------------------------*/

#ifndef SLICESLIDER_H
#define SLICESLIDER_H

#include <FL/Fl.H>
#include <FL/Fl_Scrollbar.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Window.H>

typedef void (*cb_slice)(int v);

class slideinput {
public:
    slideinput();
    void setDefaults();
    void setSlice(int v);
    int  getSlice(void) {return sliceSlider->value();}
    void setLimits(int minslice, int maxslice);
    void setWindowTitle(const char *title);
    void setValueTitle(const char *title);
    void setSliceCB(cb_slice f) {displayslice = f;} // callback
    bool visible();
    void show();
    void hide();
    friend Fl_Window *slider_panel(slideinput &p);
private:
    Fl_Window           *sliceWindow;
    Fl_Scrollbar        *sliceSlider;
    Fl_Value_Input      *sliceValue;
    int                  minSlice, maxSlice;
    cb_slice             displayslice;
};

#endif // SLICESLIDER_H
