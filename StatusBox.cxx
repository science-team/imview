/*
 * $Id: StatusBox.cxx,v 4.1 2008/10/27 15:34:11 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*  Copyright (C) 1998  Craig P. Earls

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "imview.hxx"   // might be useful if we are missing vsnprintf etc.
#include "StatusBox.hxx"
#include "machine.hxx"
#include "imunistd.h"

#define LINELENGTH 1024

StatusBox *StatusBox::_Instance=0;



StatusBox* StatusBox::Instance()
{
    if (_Instance==0){
        _Instance= new StatusBox();
    };
    
    return _Instance;
}

            
inline void StatusBox::cb_clear_i(Fl_Button*, void*) {
  list->clear();
}

void StatusBox::cb_clear(Fl_Button* o, void* v) {
  ((StatusBox*)(o->parent()->user_data()))->cb_clear_i(o,v);
}

StatusBox::StatusBox(int x, int y, int w, int h, char *l):
        Fl_Window(x, y, w, h, l)
{
    box(FL_UP_BOX);
    user_data((void*)(this));
    {
        Fl_Browser* o = list = new Fl_Browser(10, 10, 410, 120);
        o->color(FL_BLACK);
        Fl_Group::current()->resizable(o);
    }
    { Fl_Button* o = clear = new Fl_Button(10, 135, 410, 30, "&Clear");
      o->callback((Fl_Callback*)cb_clear);
    }
    end();
}

// allows multi-line messages now.
void StatusBox::status(const char *format, ...) {
    static char s1[LINELENGTH+24], s2[LINELENGTH], mess[LINELENGTH+4];
    int  i, j;
    va_list arg;

    im_va_start (arg, format);

    // add lines one by one
    vsnprintf(s2, LINELENGTH, format, arg);
    for (i=0, j= 0; ((i < LINELENGTH) && (s2[i] != '\0')); i++, j++) {
	mess[j] = s2[i];
	if (s2[i] == '\n') {
	    mess[j+1] = '\0'; // OK due to +4
	    snprintf(s1, LINELENGTH+24, "@s@B%d@C%d@.%s", FL_BLACK, FL_GREEN, mess);
	    list->add(s1);
	    j = -1; // j++ gets executed
	}
    }
    if (!shown())
	show();
    list->bottomline(list->size());
    va_end(arg);
    
}

void StatusBox::warning(const char *format, ...) {
    static char s1[LINELENGTH], s2[LINELENGTH];
    va_list arg;

    im_va_start (arg, format);
    
    vsnprintf(s2, LINELENGTH, format, arg);
    snprintf(s1, LINELENGTH, "@B%d@C%d@.%s", FL_BLACK, FL_YELLOW, s2);
    list->add(s1);
//    if (!shown())
//	show();
    list->bottomline(list->size());
    va_end(arg);
}

void StatusBox::error(const char *format, ...) {
    static char s1[LINELENGTH], s2[LINELENGTH];
    va_list arg;

    im_va_start (arg, format);
    
    vsnprintf(s2, LINELENGTH, format, arg);
    snprintf(s1, LINELENGTH, "@m@B%d@C%d@.%s", FL_BLACK, FL_RED, s2);
    list->add(s1);
    if (!shown())
	show();
    list->bottomline(list->size());
    va_end(arg);
}

/*
 * Local variables:
 *  compile-command: "make -C .. -k"
 * End:
 */
