/*
 * $Id: my_Image.hxx,v 4.0 2003/04/28 14:39:53 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * Image drawing complementary functions.
 *      
 *-----------------------------------------------------------------------*/

#ifndef MY_IMAGE_H
#define MY_IMAGE_H
void freeOffScreenPixmap(Fl_RGB_Image *b);
void fl_draw_clipped(Fl_RGB_Image *b,int ox,int oy,int x,int y,int w,int h);
void fl_draw_clipped_nobuf(Fl_RGB_Image *b,int ox,int oy,int x,int y,int w,int h);
void fl_draw_zcnb(Fl_RGB_Image *b,int ox,int oy,int ow, int oh,int x,int y,int w,int h,int isSmooth);
#endif
