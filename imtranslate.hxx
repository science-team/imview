/*
 * $Id: imtranslate.hxx,v 4.0 2003/04/28 14:39:49 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * imtranslate.H
 *
 * translate raw image data into viewable pixmaps
 *
 * Hugues Talbot	21 Jan 1998
 *      
 *-----------------------------------------------------------------------*/

#ifndef IMTRANSLATE_H
#define IMTRANSLATE_H

// external global variables that we need
// defined in imview.C
extern int             currImgNbComps, currImgWidth, currImgHeight;
extern imgtype         currImgType;
extern pixtype         currPixType;
extern unsigned short *currImgColourMap[];
extern long            currImgNbColours;
extern void          **currBuffp;

// generic functions

uchar *applyCLUTtoCurrentImage(uchar *oldata, const char *CLUTpath, int *imWidth, int *imHeight, int *imDepth);
char *getClutNameIfPresent(const char *impath);
int checkForClut(const char *clutName, const char *imageName, uchar theCLUT[][256]);
char *typeName(pixtype atype);
char *getRawDataInfo(const unsigned char *olddata, int nx, int xpos, int ypos);

void freeCurrentBuffers(void);
void readBINARY(int *imDepth, uchar **outbuf);
void readUINT1(int *imDepth, uchar **outbuf);
void readINT4(int *imDepth, uchar **outbuf);
void readUINT4(int *imDepth, uchar **outbuf);
void readDOUBLE(int *imDepth, uchar **outbuf);
void readUINT1withLUT(int *imDepth, uchar **outbuf, uchar CLUT[][256]);


#endif // IMTRANSLATE_H
