/*
 * $Id: prefio.hxx,v 1.1 2003/06/03 16:11:54 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * Saves and loads user preferences to and from ${HOME}/.imview/prefs
 *
 * Preferences themselves are just a bunch of variables in a struct. When
 * egcs supports namespaces, I may convert to this.
 *
 * Hugues Talbot	18 Oct 1998
 *
 * Modifications:
 * I made the struct a class. I may have seen the light as far as hiding
 * the actual data is concernced... Hugues Talbot	24 Oct 1998
 *
 *
 *-----------------------------------------------------------------------*/

#ifndef PREFIO_H
#define PREFIO_H

#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <ctype.h> // for case-insensitive comparison
#include <fstream>
#include <iomanip>
#include <string>
#include <map>
#include "imnmspc.hxx"

using std::string;
using std::map;
using std::ifstream;
using std::ofstream;
using std::endl;
using std::setw;
using std::ostringstream;

extern int debugIsOn;

extern string falsestr, truestr, BWstr, GREYstr, COLOURstr, CONSTRAINEDstr, UNCONSTRAINEDstr;

typedef enum {PSREND_COLOUR=0,PSREND_GREY, PSREND_BW} psrendertype;
typedef enum {ZOOM_UNCONSTRAINED = 0, ZOOM_CONSTRAINED} zoomtype;
typedef map<string,string,Nocase>::const_iterator CMI;  // const Map iterator


class prefs {
public:
    prefs();
    ~prefs();
    // data in (from the file)
    void input_prefspath(const string &s);
    void input_smoothZoomout(const string &s);
    void input_keepPoints(const string &s); 
    void input_mouseZoomType(const string &s);
    void input_overlayTransparency(const string &s);
    void input_overlayTransparency(double v);
    void input_gspath(const string &s);
    void input_psRenderRes(const string &s);
    void input_gvpath(const string &s);
    void input_psDisplayRes(const string &s);
    void input_psRenderDepth(const string &s);
    void input_psRenderSmooth(const string &s);
    void input_xorBreakLines(const string &s);
    void input_xorProfileLines(const string &s);
    void input_pollFrequency(const string &s);
    void input_requirePassword(const string &s);
    void input_localhostOnly(const string &s);
    void input_hideMenu(const string &s);
    
    // data out
    // formal (to the file)
    string& format_prefspath(void);
    string& format_smoothZoomout(void);
    string& format_keepPoints(void);      
    string& format_mouseZoomType(void);
    string& format_overlayTransparency(void);
    string& format_gspath(void);
    string& format_gvpath(void);          
    string& format_psRenderRes(void);     
    string& format_psDisplayRes(void);    
    string& format_psRenderDepth(void);   
    string& format_psRenderSmooth(void);  
    string& format_xorBreakLines(void);   
    string& format_xorProfileLines(void);
    string& format_pollFrequency(void);
    string& format_requirePassword(void);
    string& format_localhostOnly(void);
    string& format_hideMenu(void);

    // informal (to the user)
    const char      *prefspath() {return prefspath_.c_str();}
    bool             smoothZoomout() {return smoothZoomout_;}
    bool             keepPoints() {return keepPoints_;}
    zoomtype         mouseZoomType() {return mouseZoomType_;}
    double           overlayTransparency() {return overlayTransparency_;}
    const char      *gspath() {return gspath_.c_str();}
    const char      *gvpath() {return gvpath_.c_str();}
    const char      *psRenderRes() {return psRenderRes_.c_str();}
    const char      *psDisplayRes() {return psDisplayRes_.c_str();}
    psrendertype     psRenderDepth() {return psRenderDepth_;}
    bool             psRenderSmooth() {return psRenderSmooth_;}
    const char      *xorBreakLines() {return xorBreakLines_.c_str();}
    const char      *xorProfileLines() {return xorProfileLines_.c_str();}
    bool             requirePassword() {return requirePassword_;}
    bool             localhostOnly() {return localhostOnly_;}
    bool             hideMenu() {return hideMenu_;}
    double           pollFrequency(void) {return pollFrequency_;}

    // from the user
    void             pollFrequency(double v) {pollFrequency_ = v;}
    
    int              readprefs(const char *fromfile);
    int              writeprefs(const char *tofile);
    
private:
    // actual data
    // General stuff
    string            prefspath_;
    bool              smoothZoomout_, keepPoints_;
    zoomtype          mouseZoomType_;
    double            overlayTransparency_;

    // PS stuff
    string            gspath_, gvpath_, psRenderRes_, psDisplayRes_;
    psrendertype      psRenderDepth_;
    bool              psRenderSmooth_;

    // Server stuff
    double            pollFrequency_;
    bool              requirePassword_, localhostOnly_;
    
    // `expert' stuff
    bool              hideMenu_;

    // Hack stuff
    string            xorBreakLines_, xorProfileLines_;
    
    // supporting stuff
    map <string, string, Nocase> *prefitems_; // now this is bold.
    struct prefdata_  *valid_prefs;
    psrendertype      interpretPSRenderString(const string &s);
};

// out of C++: 15.5
typedef void (prefs::*pref_in_member)(const string &s);
typedef string& (prefs::*pref_out_member)(void);


typedef struct prefdata_ {
    string               prefString;
    // function to call to read the pref in
    pref_in_member       prefMethod;
    // function to call to write the pref out
    pref_out_member      prefWrite; 
    // comment string
    string               comment;
} prefdata;


#endif // PREFIO_H
