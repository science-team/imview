/*
 * $Id: spectraBox.hxx,v 4.1 2003/05/30 13:02:11 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * The spectrum display panel
 *
 * Hugues Talbot	 1 Aug 1998
 *      
 *-----------------------------------------------------------------------*/

#ifndef SPECTRABOX_H
#define SPECTRABOX_H

#include <stdlib.h>
#include <stdio.h>

#include <FL/Fl.H>
#include <FL/x.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Output.H>
#include <FL/fl_ask.H>
#include "axisBox.hxx"

#define WIDTHMARGIN 4
#define HEIGHTMARGIN 8

#ifdef WIN32
#  define MY_CURSOR_VLINE FL_CURSOR_HAND
#else
#  define MY_CURSOR_VLINE 58
#endif

class mySpectraBox : public Fl_Box {
public:
    mySpectraBox(int x, int y, int w, int h, const char *l=0)
        : Fl_Box(x,y,w,h,l) {
	spectrum = 0;
	nbSpectrumValues = 0;
	mins = maxs = 0;
    }
    int  handle(int event); // we need this for mouse control
    void handleButtonPushed();
    void handleButtonDragged();
    void handleButtonReleased();
    void setRelative() {absolute = false; recomputeLimits(); redraw();}
    void setAbsolute() {absolute = true; recomputeLimits(); redraw();}
    void setSpectra(class spectra *s) {theSpectra = s;}
    void setSpectrum(double *theSpectrum, int nbval);
    double *getSpectrum() {return spectrum;}
    int     getNbValues() {return nbSpectrumValues;}
    void recomputeLimits();
    void draw();

private:
    double  *spectrum, maxs, mins;
    int      nbSpectrumValues;
    bool     absolute;
    class spectra *theSpectra;
};

class spectra {
public:
    spectra();
    ~spectra();
    void setDefaults();
    void setData(double *s, int nb, int x, int y);
    double *getData() {return spectraBox->getSpectrum();}
    int  getNbVal() {return spectraBox->getNbValues();}
    int  getXpos() {return xIm;}
    int  getYpos() {return yIm;}
    int  visible();
    void redraw();
    void show();
    void hide();
    void displayRelative(void) {spectraBox->setRelative();}
    void displayAbsolute(void) {spectraBox->setAbsolute();}
    void setXValue(int xv) {
	static char a[10];
	sprintf(a, "%d", xv);
	xvalue->value(a);
    }
    void setYValue(double yv) {
	static char a[30];
	sprintf(a, "%g", yv);
	yvalue->value(a);
    }
    void setWindowTitle(const char *title) {spectraWindow->label(title);}
    void setXValueLabel(const char *lbl) {
	xvalue->label(lbl);
    }
    void setAxisBoxesLimits(double xminlimit, double xmaxlimit,
			    double yminlimit, double ymaxlimit);
    friend Fl_Double_Window *spectra_panel(spectra &s);

private:
    int                     xIm, yIm;
    Fl_Double_Window        *spectraWindow;
    mySpectraBox            *spectraBox;
    axisBox                 *abcissaBox, *ordinateBox;
    Fl_Button               *saveButton, *printButton;
    Fl_Return_Button        *OKButton;
    Fl_Output               *spectrumTitle, *xvalue, *yvalue;
    Fl_Check_Button         *absolute, *relative;
    
};


#endif // SPECTRABOX_H
