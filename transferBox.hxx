/*
 * $Id: transferBox.hxx,v 4.0 2003/04/28 14:40:08 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A subclass of Box which is used for brightness/contrast/etc.
 * This is a superclass for the actually interesting ones.
 * 
 *
 * Hugues Talbot	 3 May 1998
 *      
 *-----------------------------------------------------------------------*/

#ifndef TRANSFERBOX_H
#define TRANSFERBOX_H

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/fl_draw.H>


class myTransferBox : public Fl_Box {
private:
    bool   hasReleaseOccured;
    unsigned char   map[256]; 
    virtual void   trueRedrawHook() { } // called when the main window is also redrawn
    void   findExtremes(double x0, double y0, double x1, double y1,
				 double *startX, double *startY,
				 double *endX, double *endY);
    double currContrast, currBrightness, currGamma;
    double gammas[3], leftX[3], leftY[3], rightX[3], rightY[3];
    double currRGBContrast[3], currRGBBrightness[3], currRGBGamma[3];

    
protected:
    void   draw(); // I'm expecting to have to subclass this
    void   computeEndPoints(double &x0, double &y0, double &x1, double &y1);
    void   buildFunction(double x0, double y0, double x1, double y1);

public:

    myTransferBox(int x, int y, int w, int h, const char *l=0)
       : Fl_Box(x,y,w,h,l) { hasReleaseOccured = false; }

    myTransferBox(uchar b, int x, int y, int w, int h, const char *l)
        : Fl_Box(x,y,w,h,l) { hasReleaseOccured = false; }

    ~myTransferBox();
    void setParms(float contrast, float brightness, float gamma);
    void setRGBParms(double contrast[3], double brighntess[3], double gamma[3]);
    void getParms(float &contrast, float &brightness, float &gamma) {
      contrast = (float)currContrast; brightness = (float)currBrightness ; gamma = (float)currGamma;}
    virtual void getRGBParms(float &Rcontrast, float &Rbrightness, float &Rgamma,
			     float &Gcontrast, float &Gbrightness, float &Ggamma,
			     float &Bcontrast, float &Bbrightness, float &Bgamma) {}
    void causeMainWindowRedraw();
    void forceBuildMap() {
      double x0, y0, x1, y1;
      x0 = y0 = x1 = y1 = 0.0;
      computeEndPoints(x0, y0, x1, y1);
      buildFunction(x0, y0, x1, y1);
    }
    void setRelease(bool s) { hasReleaseOccured = s;}
    double getCurrGamma(void ) {return currGamma;}
    double getCurrBrightness(void) {return currBrightness;}
    double getCurrContrast(void) {return currContrast;}
    
};


#endif // TRANSFERBOX_H
