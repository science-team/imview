# data file for the Fltk User Interface Designer (fluid)
version 1.0107 
header_name {.H} 
code_name {.C}
Function {userprefs_panel(userprefs &up)} {open
} {
  Fl_Window {up.userprefsWindow} {
    label {User Preferences} open
    xywh {1699 227 575 330} type Single box ENGRAVED_BOX
    code0 {\#include "userPrefs.hxx"} visible
  } {
    Fl_Tabs {} {open
      xywh {5 25 560 245} box THIN_UP_FRAME
      code0 {o->value(up.generalGroup);}
    } {
      Fl_Group {up.generalGroup} {
        label General open
        xywh {10 55 550 210} box FLAT_BOX hide
      } {
        Fl_Round_Button {up.smoothWhenZoomoutButton} {
          label {Smooth when zooming out: }
          user_data {&up} user_data_type {userprefs*}
          callback smoothzoomout_cb
          tooltip {Linear interpolation for zoom factors < 1.0} xywh {204 110 25 25} down_box ROUND_DOWN_BOX align 4
        }
        Fl_Group {} {
          label {Default zoom behaviour is:} open
          tooltip {Constrain (or not)  zoom box to same aspect ratio as main window} xywh {200 135 300 30} align 4
        } {
          Fl_Check_Button {up.constrainZoomCheck} {
            label Constrained
            user_data {&up} user_data_type {userprefs*}
            callback constrainedzoom_cb
            xywh {200 135 30 30} type Radio down_box DIAMOND_DOWN_BOX value 1 selection_color 39 align 8
          }
          Fl_Check_Button {up.unconstrainZoomCheck} {
            label Unconstrained
            user_data {&up} user_data_type {userprefs*}
            callback unconstrainedzoom_cb
            xywh {325 135 30 30} type Radio down_box DIAMOND_DOWN_BOX selection_color 39 align 8
          }
        }
        Fl_Group {} {open
          xywh {202 75 353 30}
        } {
          Fl_Input {up.pathToPrefsInput} {
            label {Path to preference file: }
            tooltip {Changing that does nothing -- information only} xywh {205 80 280 25} textfont 4 textsize 12
            code0 {o->value("preferences");}
          }
          Fl_Button {up.prefBrowse} {
            label Browse
            user_data {&up} user_data_type {userprefs*}
            callback prefbrowse_cb
            xywh {485 80 70 25} deactivate
          }
        }
        Fl_Round_Button {up.keepPointsButton} {
          label {Keep points when switching images: }
          user_data {&up} user_data_type {userprefs*}
          callback keeppoints_cb
          tooltip {Keep points (as in pointfiles) on display} xywh {530 110 25 25} down_box ROUND_DOWN_BOX align 4
        }
        Fl_Value_Slider {up.transparencyValueSlider} {
          label {Overlay transparency:}
          user_data {&up} user_data_type {userprefs*}
          callback transparency_cb
          tooltip {0 = fully transparent ; 1= fully opaque} xywh {200 165 355 25} type Horizontal align 4 when 4 step 0.05
        }
      }
      Fl_Group {} {
        label Postscript open
        xywh {10 55 550 210} box FLAT_BOX
      } {
        Fl_Group {} {open
          xywh {170 70 385 25}
        } {
          Fl_Input {up.pathToGsInput} {
            label {Path to Ghostscript:}
            user_data {&up} user_data_type {userprefs*}
            callback pathtogsinput_cb
            tooltip {Path to postscript interpreter} xywh {170 70 315 25} textfont 4 textsize 12
            code0 {o->value("gs");}
          }
          Fl_Button {up.gsBrowse} {
            label Browse
            user_data {&up} user_data_type {userprefs*}
            callback gsbrowse_cb
            xywh {485 70 70 25} deactivate
          }
        }
        Fl_Group {} {open
          xywh {170 105 130 25} align 0
        } {
          Fl_Input {up.renderInput} {
            label {Render at:}
            user_data {&up} user_data_type {userprefs*}
            callback renderinput_cb
            tooltip {Resolution at which the file is rendered} xywh {170 105 75 25} type Int textfont 4 textsize 12
            code0 {o->value("300");}
            class Fl_Int_Input
          }
          Fl_Box {} {
            label dpi
            xywh {245 105 55 25} align 20
          }
        }
        Fl_Group {} {open
          xywh {355 105 130 25}
        } {
          Fl_Input {up.displayInput} {
            label {display at:}
            user_data {&up} user_data_type {userprefs*}
            callback displayinput_cb
            tooltip {Display resolution} xywh {355 105 75 25} type Int textfont 4 textsize 12
            code0 {o->value("150");}
            class Fl_Int_Input
          }
          Fl_Box {} {
            label dpi
            xywh {430 105 55 25} align 20
          }
        }
        Fl_Group {} {
          label {Render in:} open
          tooltip {Use colour except if speed is an issue} xywh {170 135 275 25} align 132
        } {
          Fl_Check_Button {up.colourCheck} {
            label colour
            user_data {&up} user_data_type {userprefs*}
            callback colourcheck_cb
            xywh {170 135 70 25} type Radio down_box DIAMOND_DOWN_BOX selection_color 39
          }
          Fl_Check_Button {up.greyCheck} {
            label grey
            user_data {&up} user_data_type {userprefs*}
            callback greycheck_cb
            xywh {270 135 70 25} type Radio down_box DIAMOND_DOWN_BOX selection_color 39
          }
          Fl_Check_Button {up.bwCheck} {
            label {binary b&&w}
            user_data {&up} user_data_type {userprefs*}
            callback bwcheck_cb
            xywh {355 135 90 25} type Radio down_box DIAMOND_DOWN_BOX value 1 selection_color 39
          }
        }
        Fl_Group {} {open
          xywh {105 165 325 25}
        } {
          Fl_Round_Button {up.antialiasButton} {
            label {Antialias:}
            user_data {&up} user_data_type {userprefs*}
            callback aabutton_cb
            tooltip {Smoothing at the rendering level} xywh {355 165 20 25} down_box ROUND_DOWN_BOX value 1 align 4
          }
          Fl_Round_Button {up.smoothButton} {
            label {Smooth:}
            user_data {&up} user_data_type {userprefs*}
            callback smoothbutton_cb
            tooltip {Smooth at the display level} xywh {170 165 20 25} down_box ROUND_DOWN_BOX value 1 align 4
          }
        }
        Fl_Choice {up.boundingChoice} {
          label {Bounding box:}
          user_data {&up} user_data_type {userprefs*}
          callback boundingchoice_cb open
          tooltip {Set this if the display is truncated} xywh {170 195 125 25} down_box BORDER_BOX
        } {
          MenuItem {} {
            label Automatic
            xywh {0 0 100 20}
          }
          MenuItem {} {
            label A5
            xywh {0 0 100 20}
          }
          MenuItem {} {
            label B5
            xywh {5 5 100 20} value 1
          }
          MenuItem {} {
            label A4
            xywh {0 0 100 20} value 1
          }
          MenuItem {} {
            label Letter
            xywh {0 0 100 20} value 1
          }
          MenuItem {} {
            label Legal
            xywh {10 10 100 20} value 1
          }
          MenuItem {} {
            label A3
            xywh {20 20 100 20} value 1
          }
        }
        Fl_Group {} {open
          xywh {170 230 385 25}
        } {
          Fl_Input {up.pathToPSPreviewer} {
            label {Path to previewer:}
            user_data {&up} user_data_type {userprefs*}
            callback pathtogvinput_cb selected
            tooltip {Postscript previewer (for print preview)} xywh {170 230 315 25} textfont 4 textsize 12
            code0 {o->value("ghostview");}
          }
          Fl_Button {up.gvBrowse} {
            label Browse
            user_data {&up} user_data_type {userprefs*}
            callback gsbrowse_cb
            xywh {485 230 70 25} deactivate
          }
        }
      }
      Fl_Group {} {
        label Server
        xywh {10 55 550 210} box FLAT_BOX hide
      } {
        Fl_Group {} {open
          xywh {25 70 530 55} box ENGRAVED_BOX labelfont 10
        } {
          Fl_Slider {up.pollFrequencySlider} {
            label {Poll frequency}
            user_data {&up} user_data_type {userprefs*}
            callback pollslider_cb
            tooltip {Influences speed of image loadup via server} xywh {125 75 320 25} type Horizontal align 4
          }
          Fl_Input {up.pollFrequencyInput} {
            user_data {&up} user_data_type {userprefs*}
            callback pollinput_cb
            xywh {455 75 65 25} type Float when 8
            code0 {o->value("100");}
            class Fl_Float_Input
          }
          Fl_Box {} {
            label Hz
            xywh {520 70 35 30}
          }
          Fl_Box {} {
            label 10
            xywh {115 100 25 25}
          }
          Fl_Box {} {
            label 1000
            xywh {420 100 40 25}
          }
          Fl_Box {} {
            label 100
            xywh {275 100 25 25}
          }
        }
        Fl_Group {} {
          label Security open
          xywh {25 155 155 45} box ENGRAVED_BOX labelfont 10
        } {
          Fl_Round_Button {up.securityPasswordButton} {
            label {Require password}
            user_data {&up} user_data_type {userprefs*}
            callback requirepassword_cb
            xywh {155 155 25 25} down_box ROUND_DOWN_BOX align 4 deactivate
          }
          Fl_Round_Button {up.securityLocalhostButton} {
            label {Localhost only}
            user_data {&up} user_data_type {userprefs*}
            callback localhostonly_cb
            xywh {155 175 25 25} down_box ROUND_DOWN_BOX align 4 deactivate
          }
        }
        Fl_Box {} {
          label {<- these settings not available yet}
          xywh {190 165 255 25} labelfont 3 align 20
        }
      }
      Fl_Group {} {
        label Expert open
        xywh {10 55 550 210} box FLAT_BOX hide
      } {
        Fl_Round_Button {up.hideMenuButton} {
          label {Hide menu}
          user_data {&up} user_data_type {userprefs*}
          callback hidemenu_cb
          tooltip {Will hide the main menu} xywh {100 65 25 25} down_box ROUND_DOWN_BOX align 4
        }
      }
      Fl_Group {} {
        label Debug
        xywh {10 50 550 215} box FLAT_BOX hide
      } {
        Fl_Box {} {
          label {Do not modify anything here unless you know what you are doing!!}
          xywh {51 74 500 20} box ENGRAVED_BOX labelfont 3
        }
        Fl_Round_Button {up.showDebugMsgButton} {
          label {Show debug messages}
          user_data {&up} user_data_type {userprefs*}
          callback showdebug_cb
          xywh {275 110 30 25} down_box ROUND_DOWN_BOX align 4
        }
        Fl_Round_Button {up.stopAfterDebugMsgButton} {
          label {Stop after each debug message}
          user_data {&up} user_data_type {userprefs*}
          callback stopdebug_cb
          xywh {275 135 30 25} down_box ROUND_DOWN_BOX align 4
        }
        Fl_Input {up.xorValueBreakLineInput} {
          label {XOR value for `break' lines:}
          user_data {&up} user_data_type {userprefs*}
          callback xorbreakline_cb
          xywh {280 170 120 25} textfont 4 textsize 12
          code0 {o->value("0xffffffff");}
        }
        Fl_Input {up.xorValueProfileLineInput} {
          label {XOR value for profile lines:}
          user_data {&up} user_data_type {userprefs*}
          callback xorprofileline_cb
          xywh {280 200 120 25} textfont 4 textsize 12
          code0 {o->value("0xffffffff");}
        }
        Fl_Box {} {
          label {This item won't be saved}
          xywh {295 116 265 15} labelfont 2 labelsize 12 align 20
        }
        Fl_Box {} {
          label {This item won't be saved}
          xywh {295 141 265 15} labelfont 2 labelsize 12 align 20
        }
      }
    }
    Fl_Group {} {
      xywh {265 285 300 35}
    } {
      Fl_Group {} {open
        xywh {470 285 95 35} box THIN_DOWN_FRAME
      } {
        Fl_Return_Button {up.OKButton} {
          label OK
          user_data {&up} user_data_type {userprefs*}
          callback okbutton_cb
          xywh {475 290 85 25}
        }
      }
      Fl_Button {up.saveButton} {
        label Save
        user_data {&up} user_data_type {userprefs*}
        callback savebutton_cb
        xywh {370 290 90 25}
      }
      Fl_Button {up.cancelButton} {
        label Cancel
        user_data {&up} user_data_type {userprefs*}
        callback cancelbutton_cb
        xywh {265 290 95 25}
      }
    }
  }
} 
