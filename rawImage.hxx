/*
 * $Id: rawImage.hxx,v 4.4 2009/01/26 14:39:43 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A class associated with a panel for reading arbitrary uncompressed
 * images
 *
 * Hugues Talbot	 8 Apr 2000
 * 
 *      
 *-----------------------------------------------------------------------*/

#ifndef RAWIMAGE_H
#define RAWIMAGE_H

#include <sstream>
#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>

#include "imview.hxx"


#define NBTYPES 9 // nb of pixel types
#define NBINTER 3 // nb of possible band interleaves
#define NBBYORD 4 // nb of byte orderings.

class rawimage {
public:
    rawimage();
	~rawimage();
    void setDefaults(void);
    void show(void);
    void hide(void);
    // access to private data
    void setDataSize(int v) {dataSize = v;}
    void setnx(int v) {nx = v; recomputeHeader();}
    void setny(int v) {ny = v; recomputeHeader();}
    void setnz(int v) {nz = v; recomputeHeader();}
	void setnt(int v) {nt = v; recomputeHeader();}
    void setspp(int v) {spp = v; recomputeHeader();}
    void setPixType(pixtype v) {ptype = v; recomputeHeader();}
    void setInterleave(int v) {interleave = v;}
    void setByteOrder(int v) {byteOrder = v;}
    void setHeaderSize(int v) {headerSize = v;}
    void setHeaderInput(void);
    // this method saves the user input
    void saveHeader(void);
    bool loadHeader(void);
    void tryReading(void);
    void setFileName(const char *v);
    
    friend Fl_Window *rawimage_panel(rawimage &ri);

    enum {Uchar = 0, Short, Ushort, Int, Uint, Long, Ulong, Float, Double};
    enum {Default = 0, MSB, LSB, Guess };
    enum {BIL = 0, BIP, BSQ };
    
private:
    // Dialog window
    Fl_Window         *rawimageWindow;
    //
    Fl_Box            *titleBox;
    Fl_Output         *bannerOutput;
    // dimensions
    Fl_Int_Input      *xInput, *yInput, *zInput, *tInput;
    // bands
    Fl_Int_Input      *nbBandsInput;
    Fl_Choice         *bandTypeChoice;
    Fl_Button         *assignButton;
    // pixel
    Fl_Choice         *pixelTypeChoice, *byteOrderChoice;
    // file and header
    Fl_Int_Input      *headerSizeInput;
    Fl_Output         *filesizeOutput;

    // usual buttons
    Fl_Button         *helpButton, *cancelButton, *OKButton;
    Fl_Button         *computeHeaderButton;

    // ------- other bits
    int               dataSize, headerSize;
    int               nx, ny, nz, nt;
    int               spp;
    int               interleave;
    int               byteOrder;
    pixtype           ptype;
    char              filename[1024];

    void              recomputeHeader(void);
    int               computeSkip(void);
    void              rebuildBoxTitle(void);
    void              setInputValue(Fl_Int_Input *fi, int value) {
        std::ostringstream valss;
        valss << value;
        fi->value(valss.str().c_str());
        //valss.seekp(0);
        return;
    }
    void             setPixtypeChoice(const pixtype p) {
        int index = 0;
        switch (p) {
        case IM_BINARY:
        case IM_CHAR:
            index = 0; break;
        case IM_INT2:
            index = 1; break;
        case IM_UINT2:
            index = 2 ; break;
        case IM_INT4:
            index = 3; break;
        case IM_UINT4:
            index = 4 ; break;
        case IM_FLOAT:
            index = 5 ; break;
        case IM_DOUBLE:
            index = 6 ; break;
        default :
            index = -1 ; break;
        }
        pixelTypeChoice->value(index);
    }

};

#endif // RAWIMAGE_H
