Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Hugues Talbot <Hugues.Talbot@csiro.au>
Source: http://imview.sourceforge.net/

Files: *
Copyright: 1997-2001 Hugues Talbot
License: GPL-2
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 '/usr/share/common-licenses/GPL-2'.

Files: install-sh
Copyright: 1991 Massachusetts Institute of Technology
License: Expat
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that
 the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation, and that the name of M.I.T. not be used in advertising or
 publicity pertaining to distribution of the software without specific,
 written prior permission.  M.I.T. makes no representations about the
 suitability of this software for any purpose.  It is provided "as is"
 without express or implied warranty.

Files: io/byte-swap.h
Copyright: 1996, 1997 John W. Eaton
License: GPL-2+

Files: server/socketstream.cxx server/socketstream.hxx
Copyright: 2001 Maciej Sobczak, C/C++ User Journal
License: Other-1
 you can use this code for any purpose without limitations
 (and for your own risk) as long as this notice remains

Files: stl/sstream/iotraits.h stl/sstream/sstream.h stl/sstream/sstream.cc
Copyright: 1996, 1997 Dietmar Kuehl
License: Other-2
 Permission to use, copy, modify, distribute and sell this
 software for any purpose is hereby granted without fee, provided
 that the above copyright notice appears in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation. Dietmar Kuehl makes no representations
 about the suitability of this software for any purpose. It is
 provided "as is" without express or implied warranty.

Files: old/HelpView.cxx old/HelpView.hxx
Copyright: 1997-2000 by Easy Software Products
License: Other-3
 These coded instructions, statements, and computer programs are the
 property of Easy Software Products and are protected by Federal
 copyright law.  Distribution and use rights are outblockd in the file
 "COPYING" which should have been included with this file.  If this
 file is missing or damaged please contact Easy Software Products
 at:
 .
 Attn: ESP Licensing Information
 Easy Software Products
       44141 Airport View Drive, Suite 204
       Hollywood, Maryland 20636-3111 USA
 .
       Voice: (301) 373-9600
       EMail: info@easysw.com
         WWW: http://www.easysw.com

Files: StatusBox.cxx StatusBox.hxx
Copyright: 1998  Craig P. Earl
License: GPL-2+

Files: io/readgif.cxx
Copyright: 1988 by Patrick J. Naughton <naughton@wind.sun.com>
License: Other-4
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation.

Files: server/asynchat.cxx server/asynchat.hxx server/asyncore.cxx server/asyncore.hxx
Copyright: 1996 Sam Rushing
License: Other-5
 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby
 granted, provided that the above copyright notice appear in all
 copies and that both that copyright notice and this permission
 notice appear in supporting documentation, and that the name of Sam
 Rushing not be used in advertising or publicity pertaining to
 distribution of the software without specific, written prior
 permission.
 .
 SAM RUSHING DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
 NO EVENT SHALL SAM RUSHING BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: server/semaphore.cxx server/semaphore.hxx
Copyright: 1995 UMASS CS Dept.
License: Other-6
 This code is free software; you can redistribute it and/or modify it.
 However, this header must remain intact and unchanged.  Additional
 information may be appended after this header.  Publications based on
 this code must also include an appropriate reference.
 .
 This code is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
