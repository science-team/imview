/*
 * $Id: patchlevel.h,v 4.97 2009/05/08 01:12:19 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Most of imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * This file takes care of the versioning (and of the last builder)
 * Do not edit it by hand, it gets modified by the program `buildnb'
 * in the top-level directory.
 *
 * Hugues Talbot	 8 Aug 2000
 *      
 *-----------------------------------------------------------------------*/

#define XSTR(S) STR(S)
#define STR(NAME) #NAME

/* - Changes below this line will get deleted - LEAVE THINGS AS THEY ARE -*/
const char *patchlevel=XSTR(IMVIEW_MAJOR) "." XSTR(IMVIEW_MINOR) "." XSTR(IMVIEW_MICRO);
int buildnb = 311 ;
int totalbnb = 3879 ;
const char *builder = "talbot" ;
