/*
 * $Id: progressInfo.cxx,v 4.0 2003/04/28 14:39:59 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A simple progress panel.
 *
 * Hugues Talbot	16 Mar 2000
 *      
 *-----------------------------------------------------------------------*/

#include "imnmspc.hxx" // name space and everything...

#include <stdio.h>
#include "imview.hxx"
#include "imageIO.hxx"
#include "progressInfo.hxx"

extern imageIO *IOBlackBox;

progress::progress()
{
    dbgprintf("Progress panel constructed\n");
    // must be done (Fluid depens on it)
    progressWindow = 0;
    return;
}


void progress::setDefaults()
{
    dbgprintf("Setting defaults for progress panel\n");
    progressSlider->value(0);
}

void progress::setProgress(double v)
{
    if ((v >= 0) && (v <= 100))
	progressSlider->value(v);

    return;
}

bool progress::visible()
{
    return (progressWindow->visible() != 0);
}

void progress::show()
{
    progressWindow->show();
}

void progress::hide()
{
    progressWindow->hide();
}

void okbutton_cb(Fl_Return_Button *, progress *panel)
{
    dbgprintf("Dismiss button pressed on progress panel\n");
    panel->hide();
    return;
}
