/*
 * $Id: registerSequence.cxx,v 1.1 2009/05/07 23:59:10 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A panel for reading arbitrary uncompressed images.
 *
 * Hugues Talbot	 8 Apr 2000
 *
 * A wish from the Perth group.
 *      
 *-----------------------------------------------------------------------*/

#include "imnmspc.hxx"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <sys/stat.h>
#include <map> // STL
#include <string> // STL strings
#include <iostream>
#include <fstream>
#include <sstream>
#include "imunistd.h"
#include "imview.hxx"
#include "imageIO.hxx"
#include "registerSequence.hxx"
#include "menubar.hxx" // for openimage
#include "machine.hxx"

extern imageIO *IOBlackBox;




registerpanel::registerpanel()
{
    dbgprintf("Register panel constructed\n");
    registerWindow = 0; // fluid depends on this
    return;
}

registerpanel::~registerpanel()
{
    // filename is now a char array.
}


void registerpanel::setDefaults(void)
{
        
    xminInput->value("0");
    yminInput->value("0");
    xmaxInput->value("1");
    ymaxInput->value("1");

    tfstartInput->value("0");
	tfendInput->value("30");


    // consistent corresponding values
    xmin = ymin = 0;
	xmax = ymax = 1;
    start = 0;
	end = 30;
    //filename[0] = '\0';
}

void registerpanel::show()
{
    //rebuildBoxTitle();
    registerWindow->show();
    return;
}

void registerpanel::hide()
{
    registerWindow->hide();
    return;
}



//------- callbacks --------
void xmininput_cb(Fl_Int_Input *i, registerpanel *panel)
{
    int xmin;

    xmin = atoi(i->value());
    panel->setxmin(xmin);
}

void ymininput_cb(Fl_Int_Input *i, registerpanel *panel)
{
    int ymin;

    ymin = atoi(i->value());
    panel->setymin(ymin);
}

void xmaxinput_cb(Fl_Int_Input *i, registerpanel *panel)
{
    int xmax;

    xmax = atoi(i->value());
    panel->setxmax(xmax);
}

void ymaxinput_cb(Fl_Int_Input *i, registerpanel *panel)
{
    int ymax;

    ymax = atoi(i->value());
    panel->setymax(ymax);
}

void tfend_cb(Fl_Int_Input *i, registerpanel *panel)
{
    int end;

    end = atoi(i->value());
    panel->setend(end);
}

void tfstart_cb(Fl_Int_Input *i, registerpanel *panel)
{
    int start;

    start = atoi(i->value());
    panel->setstart(start);
}

void io_output_cb(Fl_Input *, registerpanel *)
{
    dbgprintf("title output callback\n");

    return;
}

void io_input_cb(Fl_Input *, registerpanel *)
{
    dbgprintf("title input callback\n");

    return;
}

void helpbutton_cb(Fl_Button *, registerpanel *panel)
{
    dbgprintf("Help on registerpanel called\n");
    return;
}

void cancelbutton_cb(Fl_Button *, registerpanel *panel)
{
    panel->hide();
}

void okbutton_cb(Fl_Button *, registerpanel *panel)
{
    dbgprintf("OK Button pressed... \n");
    //panel->registration(); ////////////////////////////
    //panel->hide();
}

