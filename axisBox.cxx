/*
* $Id: axisBox.cxx,v 4.3 2007/06/07 13:30:14 hut66au Exp $
	*
	* Imview, the portable image analysis application
	* http://www.cmis.csiro.au/Hugues.Talbot/imview
* ----------------------------------------------------------
	*
	*  Imview is an attempt to provide an image display application
	*  suitable for professional image analysis. It was started in
	*  1997 and is mostly the result of the efforts of Hugues Talbot,
	*  Image Analysis Project, CSIRO Mathematical and Information
	*  Sciences, with help from others (see the CREDITS files for
	*  more information)
	*
	*  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
	*  supported in parts by the Australian Commonwealth Science and 
	*  Industry Research Organisation. Please see the COPYRIGHT file 
	*  for full details. Imview also includes the contributions of 
	*  many others. Please see the CREDITS file for full details.
	*
	*  This program is free software; you can redistribute it and/or modify
	*  it under the terms of the GNU General Public License as published by
	*  the Free Software Foundation; either version 2 of the License, or
	*  (at your option) any later version.
	*  
	*  This program is distributed in the hope that it will be useful,
	*  but WITHOUT ANY WARRANTY; without even the implied warranty of
	*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	*  GNU General Public License for more details.
	*  
	*  You should have received a copy of the GNU General Public License
	*  along with this program; if not, write to the Free Software
	*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
	* */

/*------------------------------------------------------------------------
	*
	* Code for axis drawing box.
	*
	* Hugues Talbot     6 Aug 1998
	*      
	*-----------------------------------------------------------------------*/

#include <stdio.h>
#include "axisBox.hxx"

void axisBox::draw(void)
{
	draw_box();
	draw_label();
	fl_color(FL_BLACK);
	if (w() > h()) {
	// single horizontal line
		fl_line(x(), y()+1, x()+w()-1, y()+1);
		if (minval != maxval) {
			char p[20];
			int  stringw, stringh;
			fl_font(FL_HELVETICA, 10);
			fl_color(FL_GRAY); // default colour
			fl_push_clip(x(),y(),w(),h());
		// draw a quick erasing box
		// nb. fl_width() returns a double and assumed to be +ve here!! k_
			fl_rectf(x()+w()-(int)(fl_width("-0.1234")+0.5)-1, y()+2, (int)(fl_width("-0.1234")+0.5), h()-2);
			fl_color(FL_BLACK);
			sprintf(p, "%.4g", minval);
			fl_draw(p, x(), y()+h()-1);
			sprintf(p, "%.4g", maxval);
			fl_measure(p, stringw, stringh);
			if (stringw <= w()/2)
				fl_draw(p, x()+w()-stringw, y()+h()-1);
			else 
				fl_draw(p, x()+w()/2, y()+h()-1); // uses at most max half of the box.
			fl_pop_clip();
		}
	} else {
	// single vertical line
		fl_line(x()+w()-1, y(), x()+w()-1, y()+h());
		if (minval != maxval) {
			char p[20];
			int  stringw, stringh;
			fl_font(FL_HELVETICA, 10);
			fl_color(FL_GRAY); // default colour
			fl_push_clip(x(),y(),w(),h());
		// draw a quick erasing box
			fl_rectf(x(), y()+h()-fl_height(), w()-1, fl_height());
			fl_color(FL_BLACK);
			sprintf(p, "%.4g", minval);
			fl_measure(p, stringw, stringh);
			if (w()-stringw-5 >= 0)
				fl_draw(p, x()+w()-stringw-5, y()+h());
			else
				fl_draw(p, x(), y()+h());

			fl_color(FL_GRAY); // default colour
			fl_rectf(x(), y(), w()-1, fl_height()+1);
			fl_color(FL_BLACK);
			sprintf(p, "%.4g", maxval);
			fl_measure(p, stringw, stringh);
			if (w()-stringw-5 >= 0)
				fl_draw(p, x()+w()-stringw-5, y()+fl_height());
			else
				fl_draw(p, x(), y()+fl_height());
			fl_pop_clip();
		}
	}
	return;
}
