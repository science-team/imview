/*
 * $Id: imageViewer.hxx,v 4.8 2008/10/09 21:20:13 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */


/*------------------------------------------------------------------------
 *
 * The ImageViewer Class
 * Basically an image class that can be opened, closed, and saved
 * to-from files.
 *
 * Hugues Talbot	27 Oct 1997
 *
 * This is my first attempt at a serious derivation. Don't laugh at
 * the code...
 *
 *-----------------------------------------------------------------------*/

#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#ifndef Fl_Object_H
#include <FL/Fl_Object.H>
#endif


#include <FL/Fl.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Hor_Slider.H>
#include <FL/Fl_Scrollbar.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Box.H>
#include <FL/fl_draw.H>

#include "imlines.hxx"
#include "imviewWindow.hxx"

// some useful defines
#define VSLIDER_WIDTH  15
#define HSLIDER_HEIGHT 15
#define STATUS_HEIGHT  21
#define STATUS_FONT    8
#define STATUS_SIZE    14

#define LEFT_BUTTON    1
#define MIDDLE_BUTTON  2
#define RIGHT_BUTTON   3

#ifdef WIN32

#  define MY_CURSOR_RIGHTARROW FL_CURSOR_DEFAULT// used to be 2
#  define MY_CURSOR_ZOOMBOX    FL_CURSOR_NWSE
#  define MY_CURSOR_CROSS      FL_CURSOR_CROSS
#  define MY_CURSOR_DOT        FL_CURSOR_HAND
#  define MY_CURSOR_KILL       FL_CURSOR_HELP
#  define MY_CURSOR_LHAND      FL_CURSOR_HAND
#  define MY_CURSOR_RHAND      FL_CURSOR_HAND
#  define MY_CURSOR_MEASURE    FL_CURSOR_MOVE
#  define MY_CURSOR_DRAW       FL_CURSOR_ARROW
#  define MY_CURSOR_SELECT     FL_CURSOR_NWSE
#  define MY_CURSOR_DEFAULT    FL_CURSOR_DEFAULT
#  define MY_CURSOR_INSERT     FL_CURSOR_INSERT

#else
/* the cursors with numerical values are not in Enumeration.H */
#  define MY_CURSOR_RIGHTARROW FL_DEFAULT_CURSOR// used to be 2
#  define MY_CURSOR_ZOOMBOX    FL_CURSOR_SE
#  define MY_CURSOR_CROSS      (Fl_Cursor)18
#  define MY_CURSOR_DOT        (Fl_Cursor)25
#  define MY_CURSOR_KILL       (Fl_Cursor)45
#  define MY_CURSOR_LHAND      (Fl_Cursor)30
#  define MY_CURSOR_RHAND      FL_CURSOR_HAND
#  define MY_CURSOR_MEASURE    (Fl_Cursor)5
#  define MY_CURSOR_DRAW       (Fl_Cursor)44
#  define MY_CURSOR_SELECT     (Fl_Cursor)40
#  define MY_CURSOR_DEFAULT    (Fl_Cursor)2  // this is the default for this application
#  define MY_CURSOR_INSERT     FL_CURSOR_INSERT

#endif

// pointer (mouse) mode
typedef enum {
    IMV_POINTER_ZOOM_MODE = 0,
    IMV_POINTER_SELECT_MODE,
    IMV_POINTER_ADDPT_MODE,
    IMV_POINTER_DRAWPT_MODE,
    IMV_POINTER_DELPT_MODE,
    IMV_POINTER_DELGRP_MODE,
    IMV_POINTER_EDITPT_MODE,
    IMV_POINTER_MEASURE_MODE
} pointermode;

// display mode
typedef enum {
    IMV_DISPLAY_WINDOW_FIT_IMG = 0, // the window follows the image size if zoomed in an out (within screen limits)
    IMV_DISPLAY_IMG_FIT_WINDOW,     // the image is made to always fit the window. Arbitrary aspect ratios are fine, zoom is disabled
    IMV_DISPLAY_DECOUPLE_WIN_IMG    // window and image are decoupled. Image is centered in window if smaller.
} displaymode;

class imageViewer: public Fl_Group {
public:
    imageViewer(int x, int y, int w, int h, const char *label=0);
    ~imageViewer();
    void setImageIO(imageIO *IO) {theImage = IO;}
    void displayCurrentImage(void);
    int close(void);
    // resize handling
    void resetDisplay(bool doRedraw=true, bool forward=true); // defined in keyboard.cxx
    bool resize(void);
    void resize(int newx, int newy, int neww, int newz);
    void vposition(int newy);  // changes the position of the image with respect to the window.
    void hposition(int newx);
    void pan(int newx, int newy);
    double  vposition(void) {return vposition_;}
    double  hposition(void) {return hposition_;}
    // handles the zoom factor
    double zoomFactor(void) { return (imageZoomFactor_); }
    double xZoomFactor(void) { return xZoomFactor_;}
    double yZoomFactor(void) { return yZoomFactor_;}
    double zoomFactor(double f, bool doRedraw=true, bool forward=true);
    double setDefaultZoomFactor(double f, bool forward=true);
    double getDefaultZoomFactor(void) {return defaultZoomFactor_;}
    double getMinZoomFactor(void) {return minZoomFactor_;}
    void   getCurrentBox(int &bx, int &by, int &bw, int &bh);
    double applyDefaultZoomFactor(void) {return zoomFactor(defaultZoomFactor_);}
    double applyMaxZoomFactor(bool useSmallerOne=true);
    double zoomToBox(int bx, int by, int bw, int bh, bool forward=true);
    // a handy couple of functions
    int currentImageWidth(void); 
    int currentImageHeight(void); 
    int currentImageDepth(void) {return (theImage->imageDepth());}
    int dataWidth(void) {return theImage->imageWidth();}
    int dataHeight(void) {return theImage->imageHeight();}
    // this HAS to be implemented
    void draw();
    // event handling method
    int handle(int event);

    // these need to be public
    int  yOnWindow(double Y);
    int  xOnWindow(double X);


    int  visibleWidth();  // returns the visible width of the object
    int  visibleHeight();  // same for height
    // point processing
    void setPointMode(bool m) {pointmode = m;}
    void deletePoint(int x, int y);
    void setPointAnnotation(void);
    void setPtColourIndex(int i) {currentPtColour_ = i;}
    // preference
    void drawSmooth(int x) {drawSmooth_ = x;}
    int  drawSmooth(void) {return drawSmooth_;}
    // useful for gamma handling
    void redraw_image();
    // line profile stuff
    void keepLine() { keepProfileLine = true;}
    void removeLine();
    void setpointermode(pointermode m) {pmode_ = m;}
    void setdisplaymode(displaymode m); // a bit too complex to put here
    displaymode getdisplaymode(void) {return dmode_;}
    // getpointermode is private
    void allowSizeConstraints(bool tf);
    // fullscreen
    bool toggle_fullscreen(void); // toggle on-and-off, returns status
    
private:
    // private variables
    Fl_RGB_Image   *imageObject;
    imageIO        *theImage;
    Fl_Scrollbar   *hscrollbar, *vscrollbar;
    imviewWindow   *parentWindow;
    Fl_Output  *statusLine;  // normally invisible status line, but can pop up sometimes
    imline     *angleLine;
    double     zoomBoxRatio;
    int        zoomBox, selectBox;     // We use this box to indicate where to zoom
    char       statusMessage[100];  // status message
    int        drawSmooth_;  // should we smooth when zooming in or out?
    int        Ox, Oy;  // origins of the image viewer
    double     vposition_, hposition_; // position of the window w. r. to the image 
    int        objectWidth, objectHeight;
    int        zoomBoxDefined, zoomBoxX, zoomBoxY, zoomBoxWidth, zoomBoxHeight;
    int        selectBoxDefined, selectBoxX, selectBoxY, selectBoxWidth, selectBoxHeight;
    int        oldBoxX, oldBoxY, oldBoxW, oldBoxH;
    double     imageZoomFactor_, minZoomFactor_, defaultZoomFactor_, xZoomFactor_, yZoomFactor_;
    int        originalWidth, originalHeight; // used when no image is visible
    int        nbFlBoxDamage, nbPointDamage; // trivial semaphores
    int        delPtX, delPtY; // coordinates of the next point to be deleted
    int        dragX, dragY, dragW, dragH;
    int        angleStartX, angleStartY;
    bool       pointmode,  zoomDragInProgress, aspectConstrained, sizeConstrained;
    bool       resizeInProgress;
    bool       lineProfileInProgress, keepProfileLine;
    pointermode  pmode_;
    displaymode  dmode_, scrollbartype_;
    int        currentPtColour_; // colour of lines and points
    bool       drawvectors_;
    
private:
    // private methods
    double computeMaxZoomFactor(bool usemin=true);
    // scrollbar stuff
    void create_hscrollbar_win_fit_img();
    void create_vscrollbar_win_fit_img();
    void create_hscrollbar_decoupled();
    void create_vscrollbar_decoupled();
    void destroy_scrollbars();
    // sub-draw methods
    void drawScrollbars_decoupled(int imw, int imh, int vw, int vh);
    void drawImage_decoupled(int imw, int imh, int imd, 
                             bool redrawbk, bool smooth, 
                             int visibleW, int visibleH, int &d);
    void drawScrollbars_win_fit_img(int imw, int imh, int &vw, int &vh);
    void drawImage_win_fit_img(int imw, int imh, int imd, 
                               bool redrawbk, bool smooth, 
                               int visibleW, int visibleH, int &d);
    void drawVectors(bool ignoreModulus);
    // these handles the size constraints
    bool sizeParent(int sizeX, int sizeY);
    void sizeConstraints(int minw, int minh);
    // this handles cursor changes
    void chg_cursor(Fl_Cursor newCursor, bool invert=false);
    // these functions transform coordinates from
    // the window referential to the image one, and
    // vice-versa.
    inline int  xInImage(int x);
    inline int  yInImage(int y);
    inline int  wInImage(int w);
    inline int  hInImage(int h);
    // adjustment functions
    inline int  xWtoW(int x);
    inline int  yWtoW(int y);
    inline int  adjW(int w);
    inline int  adjH(int h);
    // keyboard handling
    int  handleKeyboardEvent(void);
    // Button handling methods
    void handleLeftButtonPushed();
    void handleLeftButtonDragged();
    void handleLeftButtonReleased();
    void handleMiddleButtonPushed();
    void handleMiddleButtonDragged();
    void handleMiddleButtonReleased();
    void handleRightButtonReleased();
    // imageObject handling
    inline void zapImageObject(void);
    // point annotation
    void showAnnotatePanel(int X, int Y);
    pointermode getpointermode(int button); // the mode is in fact computed
};

#endif // IMAGEVIEWER_H
