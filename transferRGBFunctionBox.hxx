/*
 * $Id: transferRGBFunctionBox.hxx,v 4.0 2003/04/28 14:40:18 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A subclass of Box which is used for brightness/contrast/etc.
 * This is a superclass for the actually interesting ones.
 * 
 *
 * Hugues Talbot	 3 May 1998
 *      
 *-----------------------------------------------------------------------*/

#ifndef TRANSFER_RGB_FUNCTIONBOX_H
#define TRANSFER_RGB_FUNCTIONBOX_H

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/fl_draw.H>
#include "transferRGBBox.hxx"

class myTransferRGBFunctionBox : public myTransferRGBBox {
private:
    unsigned char   Rmap[256], Gmap[256], Bmap[256]; 
    double currRContrast, currGContrast, currBContrast;
    double currRBrightness, currGBrightness, currBBrightness;
    double currRGamma, currGGamma, currBGamma;
    
protected:
    void   draw(); // I'm expecting to have to subclass this

public:

    myTransferRGBFunctionBox(int x, int y, int w, int h, const char *l=0)
       : myTransferRGBBox(x,y,w,h,l) {  }

    myTransferRGBFunctionBox(uchar b, int x, int y, int w, int h, const char *l)
        : myTransferRGBBox(x,y,w,h,l) {  }

    void setRGBParms(float Rcontrast, float Rbrightness, float Rgamma,
		     float Gcontrast, float Gbrightness, float Ggamma,
		     float Bcontrast, float Bbrightness, float Bgamma);
    void getRGBParms(float &Rcontrast, float &Rbrightness, float &Rgamma,
		  float &Gcontrast, float &Gbrightness, float &Ggamma,
		  float &Bcontrast, float &Bbrightness, float &Bgamma) {
      Rcontrast = (float)currRContrast; Rbrightness = (float)currRBrightness ; Rgamma = (float)currRGamma;
      Gcontrast = (float)currGContrast; Gbrightness = (float)currGBrightness ; Rgamma = (float)currGGamma;
      Bcontrast = (float)currBContrast; Bbrightness = (float)currBBrightness ; Rgamma = (float)currBGamma;
    }

    // gammas
    double getCurrRGamma(void ) {return currRGamma;}
    double getCurrGGamma(void ) {return currGGamma;}
    double getCurrBGamma(void ) {return currBGamma;}
    // 
    double getCurrRBrightness(void) {return currRBrightness;}
    double getCurrGBrightness(void) {return currGBrightness;}
    double getCurrBBrightness(void) {return currBBrightness;}
    //
    double getCurrRContrast(void) {return currRContrast;}
    double getCurrGContrast(void) {return currGContrast;}
    double getCurrBContrast(void) {return currBContrast;}
};


#endif // TRANSFER_RGB_FUNCTIONBOX_H
