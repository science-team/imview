/*
 * $Id: cpostscript.hxx,v 4.6 2008/10/27 15:34:12 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */


/*------------------------------------------------------------------------
 *      cpostscript.h
 *
 * 	this file contains most definitions for color postscript
 *	output
 *
 *	by Hugues Talbot	16 Feb 1995
 *      
*-----------------------------------------------------------------------*/

#ifndef CPOSTSCRIPT_H
#define CPOSTSCRIPT_H

/* a few useful typedefs */
typedef unsigned char	byte;
#if !defined(Solaris) && !defined(DECa)
typedef unsigned char	u_char;
typedef unsigned short	u_short;
typedef unsigned long	u_long;
#endif

typedef unsigned char PIX_TYPE;


#ifndef YES
#define YES	1
#endif
#ifndef NO
#define NO	0
#endif

// types
#define PS_BINARY 1
#define PS_CHAR   2
#define PS_GREY   3

/* flags */
#define WIN_ALL	0
#define WIN_UP	1
#define WIN_LOW 2
#define WIN_EPS 3

#define ORI_PRT 0
#define ORI_LDS 1

#define A4_FMT	0
#define LE_FMT	1
#define A3_FMT	2

/* some limits */
#define MAX_NB_COPIES	500
#define MAX_IM_ACROSS	100
#define MAX_IM_DOWN	100
#define MIN_IM_AR	0.01
#define MAX_IM_AR	100.0

/* some error codes */
#define NO_IMAGE	0
#define MEM_ERROR	1
#define LACK_ZHOME	2
#define MALLOC_ERROR	-1
#define EPS_NOFILE	-2

/* the Z home directory */
#ifndef MAX_PATH
#  define MAX_PATH	1024
#endif

/* LUT constants */
#define MAXCOLOUR	256

/* first index in the page_margin array */
typedef enum {
    MARGIN_SIZE_STANDARD=0,
    MARGIN_SIZE_SMALL,
    MARGIN_SIZE_LARGE,
    MARGIN_SIZE_ZERO,
    MARGIN_SIZE_CUSTOM,
    MARGIN_SIZE_NB 
} margin_size;

/* second index in the page_margin array */
typedef enum {
    MARGIN_LEFT=0,
    MARGIN_RIGHT,
    MARGIN_TOP,
    MARGIN_BOTTOM,
    MARGIN_TITLE,
    MARGIN_BETWEEN_X,
    MARGIN_BETWEEN_Y,
    MARGIN_INDEX_NB
} margins_index;

/* margins (in points) */
#define LEFT_MARGIN	25
#define RIGHT_MARGIN	25
#define TOP_MARGIN	30
#define BOTTOM_MARGIN	75	/* to allow for the title */
#define BOTTOM_LINE	40	/* the Y value under which things get */
				/* cropped out*/

#define TEXT_START_SIZE 30
#define TEXT_MIN_SIZE	2	/* pretty unreadable already ... */

#define BETWEEN_X	5
#define BETWEEN_Y	5

#define PLINE_LENGTH	39	/* length of line in output */
				/* postscript, should be divisible by 3 (RGB) */

#ifndef PIX_TYPE_MAX
#define PIX_TYPE_MAX	255
#endif

#ifndef PIX_TYPE_MIN
#define PIX_TYPE_MIN	0
#endif

/* macros */
#define MYMIN(X, Y)	((X) > (Y)) ? (Y):(X)
#define MYMAX(X, Y)	((X) < (Y)) ? (Y):(X)


/* a structure to hold image information */
/* we want something simple capable of handling B&W and RGB images, that's all */
typedef struct himage {
    int		type;	/* binary, gray, or char */
    int		nbcomp;	/* nb of components (1 or 3 only acceptable values) */
    long	nbrows;
    long	nbcols;
    byte	needsToFreeByHand;
    PIX_TYPE	*buf0, *buf1, *buf2;
} HIMAGE;

/* a box structure */
typedef struct box {
    int 	wide;
    int		high;
    int		xmin;
    int		xmax;
    int		ymin;
    int		ymax;
} BOX;

void postscript_set_margin_index(margin_size whichMargins);
void postscript_set_custom_margins(float allmargings[MARGIN_INDEX_NB]);
void postscript_get_margins(float allmargings[MARGIN_INDEX_NB]);

/* prototype */
int compose_PostScript_code(HIMAGE *theImageList,	/* image list */
			    int	realNim,		/* real number of images */
			    double aspectRatio, 	/* aspect of the image */
			    byte paperType, 		/* type of the paper (a4, etc) */
			    byte win_type, 		/* portion of the window covered */
			    byte orientation, 		/* landscape or portrait */
			    int nbCopies, 		/* nb of copies */
			    int nbAcross, 		/* nb of images across */
			    int tnbDown, 		/* nb of images down */
			    const char *theTitle, 		/* title of the page */
			    const char *fontName, 		/* name of the font */
			    byte hasRibbon,
			    byte drawFrame,
			    FILE *tbp);

#endif // CPOSTSCRIPT_H
