/*
 * $Id: ics.hxx,v 4.0 2003/04/28 14:44:33 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */


/*************************************************************

FILE : ics.h

PURPOSE :
  Definitions of dataset structure and other stuff for 
  the ICS file format.

USAGE : #include "ics.h"

DESCRIPTION OF PARAMETERS :

AUTHOR : Damir Sudar
         Copyright (c) University of California San Francisco

HISTORY :
  3/30/89  created

******************************************************************/

#ifndef ICS_H
#define ICS_H

#define MAXDIM 5
#define SIGNED 1
#define UNSIGNED 0
#define WRITE_DIRECT 0
#define READ_DIRECT 0
#define BYTE2SHORT 1
#define SHORT2BYTE 1
#define BIT2BBIT 2
#define BIT2SBIT 3
#define BIT2LBIT 4
#define BBIT2BIT 2
#define SBIT2BIT 3
#define LBIT2BIT 4
#define FILENAME_SIZE 20
#define ORDER_SIZE 10
#define LABEL_SIZE 20
#define UNITS_SIZE 20
#define COORD_SIZE 20
#define FORMAT_SIZE 20
#define CMPS_SIZE 20
#define BYTEORDER_SIZE 16
#define SCIL_SIZE 20

typedef struct 
{
  int valid_filename;            /* if filename is valid */
  char filename[FILENAME_SIZE];  /* basename for header,data,... files */
  int valid_parameters;          /* if parameters is valid */
  int parameters;                /* number of parameters */
  int valid_order;               /* if order is valid */
  char order[MAXDIM][ORDER_SIZE];/* order of the parameters */
  int valid_sizes;               /* if sizes is valid */
  int sizes[MAXDIM];             /* dimensions of each parameter */
  int valid_sigbits;              /* if sigbits is valid */
  int sigbits;                   /* number of significant bits */
  int valid_origin;              /* if origin is valid */
  float origin[MAXDIM];          /* real world origin per parameter */
  int valid_scale;               /* if scale is valid */
  float scale[MAXDIM];           /* real world scale per parameter */
  int valid_label;               /* if label is valid */
  char label[MAXDIM][LABEL_SIZE];/* label per parameter */
  int valid_units;               /* if units is valid */
  char units[MAXDIM][UNITS_SIZE];/* measurement units per parameter */
  int valid_coord;               /* if coord is valid */
  char coord[COORD_SIZE];        /* used coordinate system */
  int valid_format;              /* if format is valid */
  char format[FORMAT_SIZE];      /* integer, float or complex */
  int valid_sign;                /* if sign is valid */
  int sign;                      /* 0 = unsigned, 1 = signed */
  int valid_compression;         /* if compression is valid */
  char compression[CMPS_SIZE];   /* compression technique used */
  int valid_byteorder;           /* if byteorder is valid */
  int byteorder[BYTEORDER_SIZE]; /* order in which bytes are stored */
  int valid_SCIL_TYPE;           /* if SCIL_TYPE is valid */
  char SCIL_TYPE[SCIL_SIZE];     /* g2d or f2d */
  int datalength;                /* number of bytes in icsdata */
  char *icsdata;                 /* extra ICS information */
} ICS;

typedef struct 
{
  int filename;            /* if filename is to be copied */
  int parameters;          /* if parameters is to be copied */
  int order;               /* if order is to be copied */
  int sizes;               /* if sizes is to be copied */
  int sigbits;             /* if sigbits is to be copied */
  int origin;              /* if origin is to be copied */
  int scale;               /* if scale is to be copied */
  int label;               /* if label is to be copied */
  int units;               /* if units is to be copied */
  int coord;               /* if coord is to be copied */
  int format;              /* if format is to be copied */
  int sign;                /* if sign is to be copied */
  int compression;         /* if compression is to be copied */
  int byteorder;           /* if byteorder is to be copied */
  int SCIL_TYPE;           /* if SCIL_TYPE is to be copied */
  int data;                /* number of bytes in icsdata is to be copied*/
} ICSC;


#endif // ICS_H
