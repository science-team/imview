/*
 * $Id: readps.hxx,v 4.4 2004/06/21 16:12:51 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*-----------------------------------------------------------------------
 * Utility function to help parse Postscript Files.
 *
 * Hugues Talbot	20 Jun 2004
 */

#ifndef READPS_HXX
#define READPS_HXX


#define DEFAULT_PS_RES       72.0 // default Postscript resolution = 72dpi

class psinfo {
 public:
    psinfo(const char *fn);
    ~psinfo();
    void getBoundingBox(float &Ox, float &Oy, float &W, float &H);
    const char *getTmpFilename(void) {return tmpfn_;}
    void parsefile(void);

 private:
    psinfo(); // not allowed to construct without a filename
    void init(void);
    void improveBB(float x1, float x2, float y1, float y2);
    const char *filename_;
    char tmpfn_[DFLTSTRLEN];
    const char *BBcs_, *DocMedcs_, *PgBBcs_ ;
    float Ox_, Oy_, W_, H_; // boundingbox
    page_size renderBB_;
};


#endif // READPS_HXX
