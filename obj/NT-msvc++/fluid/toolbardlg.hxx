// generated by Fast Light User Interface Designer (fluid) version 1.0103

#ifndef toolbardlg_hxx
#define toolbardlg_hxx
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include "toolbar.hxx"
#include <FL/Fl_Group.H>
extern void setzoommode_cb(tipButton*, toolbar*);
extern void setselectmode_cb(tipButton*, toolbar*);
extern void addpointmode_cb(tipButton*, toolbar*);
extern void deletepointmode_cb(tipButton*, toolbar*);
extern void editpointmode_cb(tipButton*, toolbar*);
extern void setmeasuremode_cb(tipButton*, toolbar*);
extern void local_rotate90right_cb(tipButton*, void*);
extern void local_rotate90left_cb(tipButton*, void*);
extern void local_flipv_cb(tipButton*, void*);
extern void local_fliph_cb(tipButton*, void*);
extern void hidetoolbar_cb(tipButton*, toolbar*);
#include <FL/Fl_Output.H>
Fl_Window* toolbar_panel(toolbar &tb);
#endif
