# Microsoft Developer Studio Project File - Name="imview" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=imview - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "imview.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "imview.mak" CFG="imview - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "imview - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "imview - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "imview - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "c:\WINNT\TEMP\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "." /I "../../../include" /I "../.." /I "H:\win32\msvc\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib wsock32.lib fltk.lib pthreadVCE.lib libtiff.lib libjpeg.lib /nologo /subsystem:windows /machine:I386 /libpath:"..\..\..\lib" /libpath:"H:\win32\msvc\lib"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "imview - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "c:\WINNT\TEMP\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "." /I "../../../include" /I "../.." /I "H:\win32\msvc\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 user32.lib gdi32.lib wsock32.lib fltkd.lib libjpeg.lib libtiff.lib pthreadVCE.lib /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib:"MSVCRT" /pdbtype:sept /libpath:"..\..\..\lib" /libpath:"H:\win32\msvc\lib"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "imview - Win32 Release"
# Name "imview - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "fluid"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\fluid\annotatedlg.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\arbitrary.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\bivdlg.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\HelpDialog.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\imginfodlg.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\printdlg.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\prntspectdlg.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\profile.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\progress.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\savedlg.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\savespectdlg.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\spectra.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\tmpmsgdlg.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\toolbardlg.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\transfer.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\transfer2.cxx
# End Source File
# Begin Source File

SOURCE=.\fluid\userprefdlg.cxx
# End Source File
# End Group
# Begin Group "io"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\io\cpostscript.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\gplot2.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\imSystem.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\prefio.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readgif.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readics.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readjpeg.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readMagick.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readpnm.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readraw.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readsocket.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readtiff.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readZimage.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\savetiff.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\saveZimage.cxx
# End Source File
# Begin Source File

SOURCE=..\..\io\writeMagick.cxx
# End Source File
# End Group
# Begin Group "server"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\server\asynchat.cxx
# End Source File
# Begin Source File

SOURCE=..\..\server\asyncore.cxx
# End Source File
# Begin Source File

SOURCE=..\..\server\imserver.cxx
# End Source File
# Begin Source File

SOURCE=..\..\server\imshared.cxx
# End Source File
# Begin Source File

SOURCE=..\..\server\interpreter.cxx
# End Source File
# Begin Source File

SOURCE=..\..\server\noimserver.cxx
# End Source File
# Begin Source File

SOURCE=..\..\server\semaphore.cxx
# End Source File
# Begin Source File

SOURCE=..\..\server\socketstream.cxx
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\annotatePoints.cxx
# End Source File
# Begin Source File

SOURCE=..\..\axisBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\bivHist.cxx
# End Source File
# Begin Source File

SOURCE=..\..\HelpView.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imageInfo.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imageIO.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imageViewer.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imdebug.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imDrawPoint.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imlines.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imtransform.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imtranslate.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imview.cxx
# End Source File
# Begin Source File

SOURCE=..\..\imviewWindow.cxx
# End Source File
# Begin Source File

SOURCE=..\..\keyboard.cxx
# End Source File
# Begin Source File

SOURCE=..\..\machine.cxx
# End Source File
# Begin Source File

SOURCE=..\..\menubar.cxx
# End Source File
# Begin Source File

SOURCE=..\..\menucb.cxx
# End Source File
# Begin Source File

SOURCE=..\..\my_Image.cxx
# End Source File
# Begin Source File

SOURCE=..\..\pointfile.cxx
# End Source File
# Begin Source File

SOURCE=..\..\printPrefs.cxx
# End Source File
# Begin Source File

SOURCE=..\..\printSpect.cxx
# End Source File
# Begin Source File

SOURCE=..\..\profileBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\progressInfo.cxx
# End Source File
# Begin Source File

SOURCE=..\..\rawImage.cxx
# End Source File
# Begin Source File

SOURCE=..\..\savePrefs.cxx
# End Source File
# Begin Source File

SOURCE=..\..\saveSpect.cxx
# End Source File
# Begin Source File

SOURCE=..\..\spectraBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\StatusBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\toolbar.cxx
# End Source File
# Begin Source File

SOURCE=..\..\transferBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\transferFunction.cxx
# End Source File
# Begin Source File

SOURCE=..\..\transferFunctionBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\transferHistoBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\transferRGBBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\transferRGBFunction.cxx
# End Source File
# Begin Source File

SOURCE=..\..\transferRGBFunctionBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\transferRGBHistoBox.cxx
# End Source File
# Begin Source File

SOURCE=..\..\userPrefs.cxx
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Group "fluid No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\fluid\annotatedlg.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\arbitrary.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\bivdlg.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\HelpDialog.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\imginfodlg.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\printdlg.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\prntspectdlg.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\profile.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\progress.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\savedlg.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\savespectdlg.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\spectra.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\tmpmsgdlg.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\toolbardlg.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\transfer.hxx
# End Source File
# Begin Source File

SOURCE=.\fluid\userprefdlg.hxx
# End Source File
# End Group
# Begin Group "io No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\io\cpostscript.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\gplot2.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\ics.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\imSystem.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\prefio.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readgif.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readics.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readjpeg.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readMagick.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readpnm.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readraw.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readsocket.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readtiff.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\readZimage.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\savetiff.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\saveZimage.hxx
# End Source File
# Begin Source File

SOURCE=..\..\io\writeMagick.hxx
# End Source File
# End Group
# Begin Group "server No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\server\asynchat.hxx
# End Source File
# Begin Source File

SOURCE=..\..\server\asyncore.hxx
# End Source File
# Begin Source File

SOURCE=..\..\server\imserver.hxx
# End Source File
# Begin Source File

SOURCE=..\..\server\imshared.hxx
# End Source File
# Begin Source File

SOURCE=..\..\server\interpreter.hxx
# End Source File
# Begin Source File

SOURCE=..\..\server\semaphore.hxx
# End Source File
# Begin Source File

SOURCE=..\..\server\socketstream.hxx
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\annotatePoints.hxx
# End Source File
# Begin Source File

SOURCE=..\..\axisBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\bivHist.hxx
# End Source File
# Begin Source File

SOURCE=..\..\HelpView.hxx
# End Source File
# Begin Source File

SOURCE=..\..\imageInfo.hxx
# End Source File
# Begin Source File

SOURCE=..\..\imageIO.hxx
# End Source File
# Begin Source File

SOURCE=..\..\imageViewer.hxx
# End Source File
# Begin Source File

SOURCE=.\imcfg.h
# End Source File
# Begin Source File

SOURCE=..\..\imDrawPoint.hxx
# End Source File
# Begin Source File

SOURCE=..\..\imlines.hxx
# End Source File
# Begin Source File

SOURCE=..\..\imnmspc.hxx
# End Source File
# Begin Source File

SOURCE=..\..\imtranslate.hxx
# End Source File
# Begin Source File

SOURCE=..\..\imview.hxx
# End Source File
# Begin Source File

SOURCE=..\..\imviewWindow.hxx
# End Source File
# Begin Source File

SOURCE=..\..\machine.hxx
# End Source File
# Begin Source File

SOURCE=..\..\menubar.hxx
# End Source File
# Begin Source File

SOURCE=..\..\my_Image.hxx
# End Source File
# Begin Source File

SOURCE=..\..\nocase.hxx
# End Source File
# Begin Source File

SOURCE=..\..\patchlevel.h
# End Source File
# Begin Source File

SOURCE=..\..\pointfile.hxx
# End Source File
# Begin Source File

SOURCE=..\..\printPrefs.hxx
# End Source File
# Begin Source File

SOURCE=..\..\printSpect.hxx
# End Source File
# Begin Source File

SOURCE=..\..\profileBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\progressInfo.hxx
# End Source File
# Begin Source File

SOURCE=..\..\rawImage.hxx
# End Source File
# Begin Source File

SOURCE=..\..\savePrefs.hxx
# End Source File
# Begin Source File

SOURCE=..\..\saveSpect.hxx
# End Source File
# Begin Source File

SOURCE=..\..\spectraBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\StatusBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\tmpMsg.hxx
# End Source File
# Begin Source File

SOURCE=..\..\toolbar.hxx
# End Source File
# Begin Source File

SOURCE=..\..\transferBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\transferFunction.hxx
# End Source File
# Begin Source File

SOURCE=..\..\transferFunctionBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\transferHistoBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\transferRGBBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\transferRGBFunction.hxx
# End Source File
# Begin Source File

SOURCE=..\..\transferRGBFunctionBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\transferRGBHistoBox.hxx
# End Source File
# Begin Source File

SOURCE=..\..\userPrefs.hxx
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
