/*
 * $Id: transferRGBBox.hxx,v 4.0 2003/04/28 14:40:15 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A subclass of Box which is used for brightness/contrast/etc.
 * This is a superclass for the actually interesting ones.
 * 
 *
 * Hugues Talbot	 3 May 1998
 *      
 *-----------------------------------------------------------------------*/

#ifndef RGBTRANSFERBOX_H
#define RGBTRANSFERBOX_H

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/fl_draw.H>
#include "imview.hxx"

class myTransferRGBBox : public Fl_Box {
private:
    bool   hasReleaseOccured;
    unsigned char  RGBmap[3][256];
    virtual void   trueRedrawHook() { } // called when the main window is also redrawn
    void   findRGBExtremes(double x0[3], double y0[3], double x1[3], double y1[3],
			   double startX[3], double startY[3],
			   double endX[3], double endY[3]);
    double currContrast[3];
    double currBrightness[3];
    double currGamma[3];
    double gammas[3], leftX[3], leftY[3], rightX[3], rightY[3];

    
protected:
    void   draw(); // I'm expecting to have to subclass this
    void   computeRGBEndPoints(double x0[3], double y0[3], double x1[3], double y1[3]);
    void   buildRGBFunction(double x0[3], double y0[3], double x1[3], double y13[3]);

public:

    myTransferRGBBox(int x, int y, int w, int h, const char *l=0)
       : Fl_Box(x,y,w,h,l) { hasReleaseOccured = false; }

    myTransferRGBBox(uchar b, int x, int y, int w, int h, const char *l)
        : Fl_Box(x,y,w,h,l) { hasReleaseOccured = false; }

    ~myTransferRGBBox();

    // standard resize
    virtual void resize(int x,int y,int w, int h) { 
	dbgprintf("TransferRGBBox %p: resized to (%dx%d+%d+%d)\n",
		  this,w,h,x,y);
	Fl_Box::resize(x,y,w,h); 
    }
	

    void setRGBParms(double contrast[3], double bright[3], double gamma[3]);
    void getRGBParms(double &RC, double &RB, double &RG,
		     double &GC, double &GB, double &GG,
		     double &BC, double &BB, double &BG) {
      RC = currContrast[0]; RB = currBrightness[0] ; RG = currGamma[0];
      GC = currContrast[1]; GB = currBrightness[1] ; GG = currGamma[1];
      BC = currContrast[2]; BB = currBrightness[2] ; BG = currGamma[2];
    }

    void causeMainWindowRedraw();
    void forceBuildMap() {
      double x0[3], y0[3], x1[3], y1[3];
      x0[0] = y0[0] = x1[0] = y1[0] = 0.0;
      x0[1] = y0[1] = x1[1] = y1[1] = 0.0;
      x0[2] = y0[2] = x1[2] = y1[2] = 0.0;
      computeRGBEndPoints(x0, y0, x1, y1);
      buildRGBFunction(x0, y0, x1, y1);
    }
    void setRelease(bool s) { hasReleaseOccured = s;}
    // gammas
    double getCurrGamma(int i) {return currGamma[i];}
    // 
    double getCurrBrightness(int i) {return currBrightness[i];}
    //
    double getCurrRContrast(int i) {return currContrast[i];}
    
};


#endif // RGBTRANSFERBOX_H
