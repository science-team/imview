dnl useful macros


dnl C++ LANGUAGE SUPPORT
dnl always a bit of a nightmare...

dnl from the autoconf-archive:
dnl http://research.cys.de/autoconf-archive
dnl now at:
dnl http://www.gnu.org/software/ac-archive/

AC_DEFUN([AC_CXX_MEMBER_TEMPLATES],
       [AC_CACHE_CHECK(whether the compiler supports member templates,
       ac_cv_cxx_member_templates,
       [AC_LANG_SAVE
        AC_LANG_CPLUSPLUS
        AC_TRY_COMPILE([
       template<class T, int N> class A
       { public:
         template<int N2> A<T,N> operator=(const A<T,N2>& z) { return A<T,N>(); }
       };],[A<double,4> x; A<double,7> y; x = y; return 0;],
        ac_cv_cxx_member_templates=yes, ac_cv_cxx_member_templates=no)
        AC_LANG_RESTORE
       ])
       if test "$ac_cv_cxx_member_templates" = yes; then
         AC_DEFINE(HAVE_MEMBER_TEMPLATES,,[define if the compiler supports member templates])
       fi
       ])

AC_DEFUN([AC_CXX_TEMPLATE_KEYWORD_QUALIFIER],
       [AC_CACHE_CHECK(whether the compiler supports use of the template keyword as a qualifier,
       ac_cv_cxx_template_keyword_qualifier,
       [AC_LANG_SAVE
        AC_LANG_CPLUSPLUS
        AC_TRY_COMPILE([
       class A { public : A() {}; template<class T> static T convert() { return T(); }
       };
       ],[double z = A::template convert<double>(); return 0;],
        ac_cv_cxx_template_keyword_qualifier=yes, ac_cv_cxx_template_keyword_qualifier=no)
        AC_LANG_RESTORE
       ])
       if test "$ac_cv_cxx_template_keyword_qualifier" = yes; then
         AC_DEFINE(HAVE_TEMPLATE_KEYWORD_QUALIFIER,,
                   [define if the compiler supports use of the template keyword as a qualifier])
       fi
       ])

AC_DEFUN([AC_CXX_TEMPLATES_AS_TEMPLATE_ARGUMENTS],
	[AC_CACHE_CHECK(whether the compiler supports templates as template arguments,
	ac_cv_cxx_templates_as_template_arguments,
	[AC_LANG_SAVE
	 AC_LANG_CPLUSPLUS
 	AC_TRY_COMPILE([
	template<class T> class allocator { public : allocator() {}; };
	template<class X, template<class Y> class T_alloc>
	class A { public : A() {} private : T_alloc<X> alloc_; };
	],[A<double, allocator> x; return 0;],
 	ac_cv_cxx_templates_as_template_arguments=yes, ac_cv_cxx_templates_as_template_arguments=no)
 	AC_LANG_RESTORE
	])
	if test "$ac_cv_cxx_templates_as_template_arguments" = yes; then
  		AC_DEFINE(HAVE_TEMPLATES_AS_TEMPLATE_ARGUMENTS,,
            	[define if the compiler supports templates as template arguments])
	fi
	])


AC_DEFUN([AC_CXX_DEFAULT_TEMPLATE_PARAMETERS],
	[AC_CACHE_CHECK(whether the compiler supports default template parameters,
	ac_cv_cxx_default_template_parameters,
	[AC_LANG_SAVE
 	AC_LANG_CPLUSPLUS
 	AC_TRY_COMPILE([
	template<class T = double, int N = 10> class A {public: int f() {return 0;}};
	],[A<float> a; return a.f();],
 	ac_cv_cxx_default_template_parameters=yes, ac_cv_cxx_default_template_parameters=no)
 	AC_LANG_RESTORE
	])
	if test "$ac_cv_cxx_default_template_parameters" = yes; then
  	AC_DEFINE(HAVE_DEFAULT_TEMPLATE_PARAMETERS,,
        	    [define if the compiler supports default template parameters])
	fi
	])


dnl This is one of my own, testing some weaknesses in egcs compilers
dnl 
AC_DEFUN([AC_CXX_TEMPLATE_CAST_ARRAY],
       [AC_CACHE_CHECK(whether the compiler accepts array type casts in templates,
       ac_cv_cxx_template_cast_array,
       [AC_LANG_SAVE
        AC_LANG_CPLUSPLUS
        AC_TRY_COMPILE([
       typedef unsigned char uchar; template <class T> void testfeeble0(void *p, T dummy) { uchar (*q)[256] = (uchar (*)[256])  p;}
       ],[uchar a[3][256], d; testfeeble0<uchar>(a,d); return 0;],
        ac_cv_cxx_template_cast_array=yes, ac_cv_cxx_template_cast_array=no)
        AC_LANG_RESTORE
       ])
       if test "$ac_cv_cxx_template_cast_array" = yes; then
         AC_DEFINE(HAVE_TEMPLATE_CAST_ARRAY,,
                   [define if your compiler accepts array type casts in templates])
       fi
       ])


dnl Testing for the presence of std::char_traits, missing in gcc 2.96 and before
dnl
AC_DEFUN([AC_CXX_STD_CHAR_TRAITS],
	[AC_CACHE_CHECK(whether the compiler supports standard char traits,
	ac_cv_cxx_standard_char_traits,
	[AC_LANG_SAVE
 	AC_LANG_CPLUSPLUS
 	AC_TRY_COMPILE([
#include <string>
	template<class charT, class traits = std::char_traits <charT> > class A {public: charT f() {return 'c';}};
	],[A<char> a; return a.f();],
 	ac_cv_cxx_standard_char_traits=yes, ac_cv_cxx_standard_char_traits=no)
 	AC_LANG_RESTORE
	])
	if test "$ac_cv_cxx_standard_char_traits" = yes; then
  	AC_DEFINE(HAVE_STANDARD_CHAR_TRAITS,,
        	    [define if the compiler supports standard char classes traits])
	fi
	])

dnl Testing for the -fpermissive stuff
dnl
AC_DEFUN([AC_CXX_X11_HEADER_PERMISSIVE],
	[AC_CACHE_CHECK(whether the compiler needs -fpermissive to compile the X11 headers,
	ac_cv_cxx_needs_permissive,
	[AC_LANG_SAVE
	AC_LANG_CPLUSPLUS
	AC_TRY_COMPILE([
#include <X11/Xlib.h>
	],,
	ac_cv_cxx_needs_permissive=no, ac_cv_cxx_needs_permissive=yes)
	AC_LANG_RESTORE
	])
	if test "$ac_cv_cxx_needs_permissive" = yes ; then
	   if test -n "$GXX" ; then 
		CPPFLAGS="-fpermissive $CPPFLAGS"
	   else
		echo "*** Compiler is not GCC, cannot fix that problem"
	   fi
	fi
	])
	
dnl Testing for the presence or absence of strstream
dnl
AC_DEFUN([AC_CXX_STRSTREAM],
	[AC_CACHE_CHECK(whether the C++ library knows about string streams,
	ac_cv_cxx_has_strstream,
	[AC_LANG_SAVE
	AC_LANG_CPLUSPLUS
	AC_TRY_COMPILE([
#include <sstream>],,
	ac_cv_cxx_has_strstream=yes,ac_cv_cxx_has_strstream=no)
	AC_LANG_RESTORE
	])
	if test "$ac_cv_cxx_has_strstream" = no ; then
		CXXFLAGS="-I$srcdirfull/stl/sstream $CXXFLAGS"
	fi
	])


dnl check for presence of Xinerama
dnl
AC_DEFUN([AC_X11_HAS_XINERAMA],
	[AC_CACHE_CHECK(check for the xinerama extension,
	ac_x11_has_xinerama,
	[AC_LANG_SAVE
	AC_LANG_CPLUSPLUS
	AC_TRY_COMPILE([
#include <X11/Xlib.h>
#include <X11/extensions/Xinerama.h>
	],[ (void) XineramaIsActive(0); return 0; ],
	ac_x11_has_xinerama=yes,ac_x11_has_xinerama=no)
	AC_LANG_RESTORE
	])
	if test "$ac_x11_has_xinerama" = yes ; then
		XINERAMA="-lXinerama"
		AC_DEFINE(HAVE_XINERAMA,,
			[define if your multiple-screen system supports xinerama])
	else
		XINERAMA=""
	fi
	])

dnl I'm starting to HATE ImageMagick. They change the API for no reasons all the time.
dnl


AC_DEFUN([AC_MAGICK_REASON],
	[AC_CACHE_CHECK(whether ImageMagick uses the new exception structure,
	ac_cv_magick_reason,
	[AC_LANG_SAVE
	AC_LANG_CPLUSPLUS
	AC_TRY_COMPILE([
#include <stdio.h>
#include <sys/types.h> 
#include <magick/api.h>],[
	ExceptionInfo exception; 
	MagickWarning(exception.severity,exception.reason,exception.description);
	return 0;],
	ac_cv_magick_reason=yes,ac_cv_magick_reason=no)
	AC_LANG_RESTORE
	])
	if test "$ac_cv_magick_reason" = yes ; then
		AC_DEFINE(MAGICK_USES_REASON,,[define if ImageMagick uses the new exception structure])
	fi
	])



AC_DEFUN([AC_MAGICK_INIT],
	[AC_CACHE_CHECK(whether ImageMagick uses InitializeMagick,
	ac_cv_magick_init,
	[AC_LANG_SAVE
	AC_LANG_CPLUSPLUS
	AC_TRY_COMPILE([
#include <stdio.h>
#include <sys/types.h> 
#include <magick/api.h>],[
	InitializeMagick(NULL);
        return 0;],
	ac_cv_magick_init=yes,ac_cv_magick_init=no)
	AC_LANG_RESTORE
	])
	if test "$ac_cv_magick_init" = yes ; then
		AC_DEFINE(MAGICK_USES_INIT,,[define if ImageMagick uses InitializeMagick()])
	fi
	])

AC_DEFUN([AC_MAGICK_NEEDS_MORE_EXCEPTION],
	[AC_CACHE_CHECK(whether ImageMagick passes around an extra exception reference all over the place,
	ac_cv_magick_needs_more_exception,
	[AC_LANG_SAVE
	AC_LANG_CPLUSPLUS
	AC_TRY_COMPILE([
#include <stdio.h>
#include <sys/types.h> 
#include <magick/api.h>],[
	ExceptionInfo exception;
	Image *image;
	
	IsGrayImage(image, &exception);
        return 0;],
	ac_cv_magick_needs_more_exception=yes,ac_cv_magick_needs_more_exception=no)
	AC_LANG_RESTORE
	])
	if test "$ac_cv_magick_needs_more_exception" = yes ; then
		AC_DEFINE(MAGICK_NEEDS_MORE_EXCEPTION,,[define if ImageMagick requires extra reference to exceptions in arguments])
	fi
	])

AC_DEFUN([AC_ACCEPT_USES_SOCKLEN_T],
	[AC_CACHE_CHECK(whether accept uses socklen_t,
	ac_cv_accept_uses_socklen_t_init,
	[AC_LANG_SAVE
	AC_LANG_C
	AC_TRY_COMPILE([
#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>],[
	accept (1, NULL, (socklen_t*)NULL);
        return 0;],
	ac_cv_accept_uses_socklen_t_init=yes,acc_cv_accept_uses_socklen_t_init=no)
	AC_LANG_RESTORE
	])
	if test "$ac_cv_accept_uses_socklen_t_init" = yes ; then
		AC_DEFINE(ACCEPT_USES_SOCKLEN_T,,[define if accept*uses socklen_t])
	fi
	])


# OK, so gcc-3.1 complains about adding -I<something> when <something> is 
# a system path. Fair enough, but sometimes you need to add this something
# and you have no idea that it is a system path unless gcc tells you (this
# depends on the way gcc has been installed).
# This macro checks for it.
AC_DEFUN([GCC_ACCEPTS_INCLUDE_DIR],
	[AC_CACHE_CHECK(whether gcc bitches when told to look inside INCLUDE_DIR,
	ac_cv_gcc_complains_about_system_headers,
	[AC_LANG_SAVE
	AC_LANG_C
	CPPSAVE=$CPPFLAGS
	CPPFLAGS="$CPPFLAGS -I$INCLUDE_DIR -Werror"
	AC_TRY_COMPILE([
#include <stdio.h>], [
	printf("hello world\n");],
	ac_cv_gcc_complains_about_system_headers=no,ac_cv_gcc_complains_about_system_headers=yes)
	AC_LANG_RESTORE
	CPPFLAGS=$CPPSAVE
        ])
	if test "$ac_cv_gcc_complains_about_system_headers" = no ; then
		CPPFLAGS="$CPPFLAGS -I$INCLUDE_DIR"
	else
		CPPFLAGS="$CPPFLAGS -isystem $INCLUDE_DIR" 
	fi
	])
	
# bitchy gcc-3.1 complains about `deprecated header' which is a pain
# when you *have* to support older compiler. Shut off the complaint if 
# necessary
AC_DEFUN([GCC_ACCEPTS_DEPRECATED_HEADERS],
	[AC_CACHE_CHECK(whether gcc bitches when including some deprecated headers,
	ac_cv_gcc_complains_about_deprecated_headers,
	[AC_LANG_SAVE
	AC_LANG_CPLUSPLUS
	CPPSAVE=$CPPFLAGS
	CPPFLAGS="$CPPFLAGS -I$INCLUDE_DIR -Werror"
	AC_TRY_COMPILE([
#include <streambuf.h>
#include <iostream.h>], [
	std::cout << "Hello world\n";],
	ac_cv_gcc_complains_about_deprecated_headers=no,ac_cv_gcc_complains_about_deprecated_headers=yes)
	AC_LANG_RESTORE
	CPPFLAGS=$CPPSAVE
        ])
	if test "$ac_cv_gcc_complains_about_deprecated_headers" = yes ; then
		CXXFLAGS="$CXXFLAGS -Wno-deprecated" 
	fi
	])
