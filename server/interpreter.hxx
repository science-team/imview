/*
 * $Id: interpreter.hxx,v 4.2 2003/09/07 23:53:33 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */


/*------------------------------------------------------------------------
 *
 *
 * A minuscule interpreter for Imview, accessed through the socket
 * interface.
 * The idea is to be able to control the GUI from the command line,
 * so that:
 *    1- remote control of imview from another application is easy
 *    2- linking two or more imviews is a possibility
 *    3- regression testing of imview is possible.
 *
 * Hugues Talbot	 1 Mar 2000
 *      
 *-----------------------------------------------------------------------*/

#ifndef INTERPRETER_H
#define INTERPRETER_H

#ifdef HAVE_PTHREADS

#include <vector>
#include <string>
#include <list>
#include <map>
#include <bitset>
#include <stdlib.h>
#include <string.h>
#include "imview.hxx"
#include "../nocase.hxx"
#include "imserver.hxx"
#include "socketstream.hxx"

using std::vector;
using std::string;
using std::list;
using std::map;
using std::bitset;

#define HEADER_ID_SIZE 8 // so that we align even for doubles

// -------- all the actions ----------
class action {
public:
    virtual int perform(vector<string> &l, string &result)
	{ cerr << "default action!" << endl; return 1;}  // with a list of arguments
    virtual ~action() {}
};

typedef map<string,action*,Nocase>::iterator AMI;  // action Map iterator

class complain:public action {
public:
    int perform(vector<string> &l, string &result);
};

class interpreter_help:public action {
public:
    int perform(vector<string> &l, string &result);
};

class deny:public action{
public:
    int perform(vector<string> &l, string &result);
};

// much-needed authentication
class user:public action{
    string server_user; // who is running the server now?
    // static bool   matches;
public:
    user(void); // constructor
    // static bool unauthenticate(void) {matches = false;}
    // static bool authenticated(void) {return matches;}
    int perform(vector<string> &l, string &result);
};

class put:public action {
private:
    static inline unsigned int swapid(int id);
protected:
    static vector<IMAGE_HEADER *> imh;
    static unsigned int id_pool;
public:
    virtual int perform(vector<string> &l, string &result);
    static int callback(void *buf, unsigned long nb); // will be called when the data is received
    static IMAGE_HEADER *find_header(const char *name);
    static IMAGE_HEADER *find_header(unsigned int given_id);
    static void delete_header(const char *name);
    static void delete_header(unsigned int given_id);
    static void delete_header(IMAGE_HEADER *myeader);
};

class ppath:public action {
public:
    int perform(vector<string> &l, string &result);
};

// declaration for mput is in imshared.H

class cmap:public action {
public:
    int perform(vector<string> &l, string &result);
};

class move:public action {
public:
    int perform(vector<string> &l, string &result);
};

class size:public action {
public:
    int perform(vector<string> &l, string &result);
};

class load:public action {
public:
    int perform(vector<string> &l, string &result);
};

class zoom:public action {
public:
    int perform(vector<string> &l, string &result);
};

class pan:public action {
public:
    int perform(vector<string> &l, string &result);
};

class reset:public action {
public:
    int perform(vector<string> &l, string &result);
};

class close_image:public action {
public:
    int perform(vector<string> &l, string &result);
};

class quit:public action {
public:
    int perform(vector<string> &l, string &result);
};

class killme:public action {
public:
    int perform(vector<string> &l, string &result);
};

class print_pointfile:public action {
public:
    int perform(vector<string> &l, string &result);
};

class set_appname:public action {
public:
    int perform(vector<string> &l, string &result);
};

class set_pointfile:public action {
public:
    int perform(vector<string> &l, string &result);
};

class load_pointfile:public action {
public:
    int perform(vector<string> &l, string &result);
};

class save_pointfile:public action {
public:
    int perform(vector<string> &l, string &result);
};

class clear_pointfile:public action {
public:
    int perform(vector<string> &l, string &result);
};

class iconize_window:public action {
public:
    int perform(vector<string> &l, string &result);
};

class raise_window:public action {
public:
    int perform(vector<string> &l, string &result);
};

class hide_window:public action {
public:
    int perform(vector<string> &l, string &result);
};

class show_window:public action {
public:
    int perform(vector<string> &l, string &result);
};

class set_variable:public action {
public:
    int perform(vector<string> &l, string &result);
};

class get_variable:public action {
public:
    int perform(vector<string> &l, string &result);
};

class list_variables:public action {
public:
    int perform(vector<string> &l, string &result);
};

class tokenize:public action {
public:
    int perform(vector<string> &l, string &result);
};

// link to another imview
class imlink:public action {
public:
    int perform(vector<string> &l, string &result);
};

// unlink from another imview
class imunlink:public action {
public:
    int perform(vector<string> &l, string &result);
};

// -------- The variables  ---------

class variable {
public:
    virtual void set(const string &stringval) = 0;
    virtual string &get(void)  = 0;
    virtual string list(void) = 0;
    virtual ~variable() {}
};

class useoffscreenbuf:public variable {
public:
    void set(const string &stringval);
    string &get(void);
    string list(void);
};

class lutwraparound:public variable {
public:
    void set(const string &stringval);
    string &get(void);
    string list(void);
};

class syncdisplay:public variable {
public:
    void set(const string &stringval);
    string &get(void);
    string list(void);
};

class connectionlist:public variable {
public:
    void set(const string &stringval) {/* does nothing */};
    string &get(void);
    string list(void);
};

class imagewindowfit:public variable {
public:
    void set(const string &stringval);
    string &get(void);
    string list(void);
};

// -- conversion helper functions
void fromstr_convert(const string &strval, bool &value);
string &tostr_convert(bool value);
void fromstr_convert(const string &strval, int &value);
string &tostr_convert(int value);

// -------- The interpreter ---------

// connections to other imviews
struct connection {
    string            address;
    int               portnumber;
    TCPClientStream  *stream;
    connection() 
        : stream(0) {};
    ~connection() { delete stream ;}
};


// this class should be a singleton
// this class manages the links with other imviews
// i.e: 
//  - remembers which other imviews we are currently connected to
//  - sends actions to remote imviews
class linkmanager {
    typedef vector<connection *>::iterator connectionIterator;
    enum DupActions {
        ActionZoom = 0, // pan when the controller pans
        ActionPan,      // zoom ...
        ActionMove,     // move window around (keep same distance)
        ActionResize,   // resize all windows
        ActionDialogs,  // pop same dialogs
        ActionProfile,  // draw profiles
        ActionColormap, // apply same colormap
        ActionQuit,     // quit application
        ActionMax       // MUST BE THE LAST ACTION
    };
    vector <connection *> otherimview;
    bitset <ActionMax>  duplicate_action; // actions to duplicate to other imviews when connected
    
    int getresponse(TCPClientStream *stream);

public:
    linkmanager() {    
	duplicate_action = 0xff; // all actions enabled 
    }
    ~linkmanager() {
	// kill all connections
	for (connectionIterator i = otherimview.begin() ; i != otherimview.end() ; ++i)
	    delete *i;
    }
    void save_connection(connection *c) {otherimview.push_back(c);}
    int nb_connections(void) { return otherimview.size(); }
    string print_connections(void);
    // real work under here
    int zoombox(int x, int y, int w, int h);
    int zoomfactor(double f);
    int setDefaultZoomFactor(double df);
    int resetDisplay(void);
};

// this should be a singleton
class interpreter {

private:
    complain ourcomplaint;
    map<string,action*, Nocase> tokens;
    map<string,variable*, Nocase> varnames;
    map<string,imgtype, Nocase> tr_img; // this is only the declaration!!
    map<string,pixtype, Nocase> tr_pix;
    vector <connection *> otherimview;
    
public:
    interpreter();
    ~interpreter();
    imgtype translate_img(string &type) {
	map<string,imgtype,Nocase>::iterator p = tr_img.find(type);
	if (p != tr_img.end())
	    return p->second;
	else
	    return IM_ERROR;
    }
    pixtype translate_pix(string &type) {
	map<string,pixtype,Nocase>::iterator p = tr_pix.find(type);
	if (p != tr_pix.end())
	    return p->second;
	else
	    return IM_INVALID;
    }
    void evaluate(string &s, vector<string> &args, string &result);
    void parseCommand(string &command, string &result);
    int setVariable(const string &varname, const string &value);
    int getVariable(const string &varname, string &value);
    
    string &listVariables(void);
};

#else // HAVE_PTHREADS

// stubs

class interpreter {
public:
    //    void evaluate(string &s, vector<string> &args, string &result);
    //void parseCommand(string &command, string &result);
    //int setVariable(const string &varname, const string &value);
    //int getVariable(const string &varname, string &value);
    
};


class linkmanager {
public:
    int zoombox(int x, int y, int w, int h) { return 1;}
    int zoomfactor(double f) { return 1;}
    int setDefaultZoomFactor(double df) { return  1;}
    int resetDisplay(void) { return 1;}
};

#endif // HAVE_PTHREADS

#endif // INTERPRETER_H
